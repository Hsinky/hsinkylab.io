import {
	defaultTheme
} from '@vuepress/theme-default'
import {
	getDirname,
	path
} from '@vuepress/utils'
import {
	registerComponentsPlugin
} from '@vuepress/plugin-register-components'
import {
	pwaPlugin
} from '@vuepress/plugin-pwa'
import {
	mdEnhancePlugin
} from "vuepress-plugin-md-enhance";

import {
	containerPlugin
} from '@vuepress/plugin-container'

import {
	backToTopPlugin
} from '@vuepress/plugin-back-to-top'
import {
	tocPlugin
} from '@vuepress/plugin-toc'

const __dirname = getDirname(
	import.meta.url)
export default {
	lang: 'zh-CN',
	title: '李欣琪 Hsinky Li',
	description: '李欣琪,hsinky li,newui,new-ui,vue,react,javascript,ui库,uniapp,uni-app,uview,graceui,iview,vant,mui,colorui',
	head: [
		['link', {
			rel: 'manifest',
			href: '/manifest.webmanifest'
		}],
		['meta', {
			name: 'theme-color',
			content: '#3eaf7c'
		}],
		['meta', {
			name: 'keywords',
			content: '李欣琪,newui,new-ui,vue,react,javascript,ui库,uniapp,uni-app,uview,graceui,iview,vant,mui,colorui'
		}],
		['meta', {
			name: 'baidu-site-verification',
			content: 'code-N5QjQ4k4cw'
		}],
		['link', {
			rel: 'icon',
			href: 'https://gitlab.com/uploads/-/system/user/avatar/3730219/avatar.png'
		}],
	],
	theme: defaultTheme({
		logo: 'https://gitlab.com/uploads/-/system/user/avatar/3730219/avatar.png',
		contributors: false,
		lastUpdated: false,
		sidebarDepth: 1,
		navbar: [{
				text: '首页',
				link: '/'
			},
			{
				text: 'NewUI',
				children: [{
						text: '预览',
						link: '/newUI/component/guide',
					},
					{
						text: '指南',
						link: '/newUI/info/guide',
						activeMatch: '^/newUI/info/',
					},
					{
						text: '组件',
						link: '/newUI/component/guide',
						activeMatch: `^/newUI/component/`,
					},
					{
						text: 'JS库',
						link: '/newui/jssdk/guide',
						activeMatch: `^/newUI/jssdk/`,
					},
				]
			},
			{
				text: 'Uni-App',
				link: '/uniapp/guide',
				activeMatch: `^/uniapp/`,
			},
			{
				text: 'CSS',
				children: [{
						text: '命名规范',
						link: '/css/namespace'
					},
					{
						text: '教程文档',
						link: '/css/document'
					},
					{
						text: '问题解答',
						link: '/css/issue'
					}
				]
			},
			{
				text: 'JavaScript',
				link: '/javascript/guide'
			},
			{
				text: 'Vue',
				link: '/vue/guide',
				activeMatch: `^/vue/`,
				children: [{
						text: 'Vue',
						link: '/vue/guide',
					},
					{
						text: 'VueRouter',
						link: '/vue/router/guide',
					},
					{
						text: 'Pinia',
						link: '/vue/pinia/guide',
					},
					{
						text: 'Nuxt',
						link: '/vue/nuxt/qa',
					},
					{
						text: 'VuePress',
						link: '/vue/vuepress/guide',
					}
				]
			},
			{
				text: 'Angular',
				link: '/angular/qa',
				activeMatch: `^/angular/`,
			},
			{
				text: 'TypeScript',
				link: '/typescript/guide',
				activeMatch: `^/typescript/`,
			},
			{
				text: 'Electron',
				link: '/electron/guide',
			},
			{
				text: '笔记',
				link: '/note/guide',
				activeMatch: `^/note/`,
			},
		],
		sidebar: {
			'/angular': [{
				text: 'Angular',
				children: [
					'/angular/qa',
				]
			}],
			'/css': [{
				text: 'CSS',
				children: [
					'/css/namespace',
					'/css/document',
					'/css/issue'
				]
			}],
			'/vue': [{
				text: 'Vue',
				children: [
					'/vue/guide',
					'/vue/router/guide',
					'/vue/pinia/guide',
					'/vue/nuxt/qa',
					'/vue/vuepress/guide',
				]
			}],
			'/typescript': [{
				text: 'TypeScript',
				children: [
					'/typescript/guide',
					'/typescript/basic-types',
					'/typescript/enum',
					'/typescript/functions',
					'/typescript/interfaces',
					'/typescript/generics',
					'/typescript/tsconfig-json',
					'/typescript/summary',
				]
			}],
			'/electron': [{
				text: 'Electron',
				children: [
					'/electron/guide',
					'/electron/electron-builder',
					'/electron/youhuabaiping'
				]
			}],
			'/webSocket': [{
				text: 'WebSocket',
				children: ['/webSocket/guide']
			}],
			'/node': [{
				text: 'NodeJs',
				children: ['/node/qa']
			}],
			'/newUI/info': [{
				text: 'New UI使用指南',
				children: [
					'/newUI/info/guide',
					'/newUI/info/install',
					'/newUI/info/setting'
				]
			}],
			'/newUI/component/': [{
				text: 'New UI组件库',
				children: [{
						text: '预览',
						link: '/newUI/component/guide'
					},
					{
						text: '页面布局',
						children: ['/newUI/component/n-page']
					},
					{
						text: '表单组件',
						children: [
							'/newUI/component/n-button',
							'/newUI/component/radio',
							'/newUI/component/checkbox'
						]
					},
					{
						text: '原子样式组件',
						children: [
							// '/newUI/component/button',
							'/newUI/component/radio',
							'/newUI/component/checkbox'
						]
					},
					{
						text: '扩展组件',
						children: [
							'/newUI/component/n-card',
							'/newUI/component/n-icon',
							'/newUI/component/n-group',
							'/newUI/component/n-divider',
							'/newUI/component/n-lottie',
							'/newUI/component/n-tabs',
						]
					},
				]
			}],
			'/newUI/jssdk/': [{
				text: 'New UI JS库',
				children: ['/newUI/jssdk/guide',
					{
						text: '函数库',
						children: ['/newUI/jssdk/timeFormat']
					}
				]
			}],
			'/uniapp/': [{
				text: 'UNI-APP',
				children: ['/uniapp/guide', '/uniapp/uniCloud']
			}],
			'/note/': [{
				text: '其他',
				children: [{
						text: '线程',
						link: '/note/guide'
					},
					{
						text: 'gitlLab',
						link: '/note/gitlab/guide'
					},
				]
			}],
		}
	}),
	plugins: [
		//自动引入vue组件，https://v2.vuepress.vuejs.org/zh/reference/plugin/register-components.html#%E5%AE%89%E8%A3%85
		registerComponentsPlugin({
			// 配置项
			componentsDir: path.resolve(__dirname, './components'),
			components: {
				threejsDemo1: path.resolve(__dirname, './components/threejs/demo1.vue'),
			},
		}),
		//自定义container
		containerPlugin({
			// 配置项
			type: 'demo',
		}),
		containerPlugin({
			// 配置项
			type: 'doc',
		}, {
			type: 'full'
		}),
		//PWA
		pwaPlugin({
			// 配置项
			skipWaiting: true,
		}),
		backToTopPlugin(),
		'vuepress-plugin-baidu-autopush',
		mdEnhancePlugin({
			// 你的选项
			tabs: true,
		}),
		tocPlugin({
			includeLevel: [1]
		})
	],
}