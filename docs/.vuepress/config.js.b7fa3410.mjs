// docs/.vuepress/config.js
import {
  defaultTheme
} from "@vuepress/theme-default";
import {
  getDirname,
  path
} from "@vuepress/utils";
import {
  registerComponentsPlugin
} from "@vuepress/plugin-register-components";
import {
  pwaPlugin
} from "@vuepress/plugin-pwa";
import {
  containerPlugin
} from "@vuepress/plugin-container";
import {
  backToTopPlugin
} from "@vuepress/plugin-back-to-top";
import enhance from "vuepress-plugin-md-enhance";
var __vite_injected_original_import_meta_url = "file:///D:/Works/blog/docs/.vuepress/config.js";
var { mdEnhancePlugin } = enhance;
var __dirname = getDirname(
  __vite_injected_original_import_meta_url
);
var config_default = {
  lang: "zh-CN",
  title: "\u674E\u6B23\u742AHsinkyLi",
  description: "\u674E\u53C9\u53C9,\u674E\u6B23\u742A,newui,new-ui,vue,react,javascript,ui\u5E93,uniapp,uni-app,uview,graceui,iview,vant,mui,colorui",
  head: [
    ["meta", {
      name: "keywords",
      content: "\u674E\u53C9\u53C9,\u674E\u6B23\u742A,newui,new-ui,vue,react,javascript,ui\u5E93,uniapp,uni-app,uview,graceui,iview,vant,mui,colorui"
    }],
    ["link", {
      rel: "manifest",
      href: "/manifest.webmanifest"
    }],
    ["meta", {
      name: "theme-color",
      content: "#3eaf7c"
    }],
    ["meta", {
      name: "baidu-site-verification",
      content: "code-2RsDrw8jl2"
    }],
    ["link", {
      rel: "icon",
      href: "https://gitlab.com/uploads/-/system/user/avatar/3730219/avatar.png"
    }]
  ],
  theme: defaultTheme({
    logo: "https://gitlab.com/uploads/-/system/user/avatar/3730219/avatar.png",
    contributors: false,
    lastUpdated: false,
    sidebarDepth: 1,
    navbar: [
      {
        text: "\u9996\u9875",
        link: "/"
      },
      {
        text: "NewUI",
        children: [
          {
            text: "\u6307\u5357",
            link: "/newUI/info/guide",
            activeMatch: "^/newUI/info/"
          },
          {
            text: "\u7EC4\u4EF6",
            link: "/newUI/component/card",
            activeMatch: `^/newUI/component/`
          },
          {
            text: "JS\u5E93",
            link: "/newui/jssdk/guide",
            activeMatch: `^/newUI/jssdk/`
          }
        ]
      },
      {
        text: "WebGL",
        link: "/webgl/guide",
        activeMatch: `^/webgl/`,
        children: [
          {
            text: "ThreeJs",
            link: "/webgl/threejs/guide",
            activeMatch: "^/webgl/threejs/"
          },
          {
            text: "BabylonJs",
            link: "/webgl/babylonjs/guide",
            activeMatch: `^/webgl/babylonjs/`
          }
        ]
      },
      {
        text: "uni-app",
        link: "/uniapp/guide",
        activeMatch: `^/uniapp/`
      },
      {
        text: "Vite-Vue3-TypeScript-Electron",
        link: "/Vite-Vue3-TypeScript-Electron/guide",
        activeMatch: `^/Vite-Vue3-TypeScript-Electron/`
      },
      {
        text: "WebSocket",
        link: "/webSocket/guide",
        activeMatch: `^/webSocket/`
      },
      {
        text: "\u7B14\u8BB0",
        link: "/note/guide",
        activeMatch: `^/note/`
      },
      {
        text: "\u8054\u7CFB\u6211",
        link: "/about/guide",
        activeMatch: `^/about/`
      }
    ],
    sidebar: {
      "/newUI/info": [{
        text: "New UI\u4F7F\u7528\u6307\u5357",
        children: [
          "/newUI/info/guide",
          "/newUI/info/install",
          "/newUI/info/setting"
        ]
      }],
      "/newUI/component/": [{
        text: "New UI\u7EC4\u4EF6\u5E93",
        children: [
          {
            text: "\u524D\u8A00",
            link: "/newUI/component/guide"
          },
          {
            text: "\u9875\u9762\u5E03\u5C40",
            children: [{
              text: "Page \u9875\u9762",
              link: "/newUI/component/page"
            }]
          },
          {
            text: "\u539F\u751F+class\u4FEE\u9970\u7EC4\u4EF6",
            children: [
              {
                text: "Button \u6309\u94AE",
                link: "/newUI/component/button"
              },
              {
                text: "Radio \u5355\u9009\u6309\u94AE",
                link: "/newUI/component/radio"
              },
              {
                text: "Checkbox \u590D\u9009\u6309\u94AE",
                link: "/newUI/component/checkbox"
              }
            ]
          },
          {
            text: "\u6269\u5C55\u7EC4\u4EF6",
            children: [
              "/newUI/component/card",
              "/newUI/component/icon",
              "/newUI/component/group",
              "/newUI/component/divider"
            ]
          }
        ]
      }],
      "/newUI/jssdk/": [{
        text: "New UI JS\u5E93",
        children: [
          {
            text: "\u524D\u8A00",
            link: "/newUI/jssdk/guide"
          },
          {
            text: "\u51FD\u6570\u5E93",
            children: [{
              text: "\u65F6\u95F4",
              children: [{
                text: "timeFormat \u65F6\u95F4\u683C\u5F0F\u5316",
                link: "/newUI/jssdk/timeFormat"
              }]
            }]
          }
        ]
      }],
      "/uniapp/": [{
        text: "uni-pp",
        children: [
          {
            text: "uniapp",
            link: "/uniapp/guide"
          },
          {
            text: "uniCloud",
            link: "/uniapp/uniCloud"
          }
        ]
      }],
      "/note/": [{
        text: "\u7B14\u8BB0",
        children: [
          {
            text: "\u7EBF\u7A0B",
            link: "/note/guide"
          },
          {
            text: "vuepress",
            link: "/note/vuepress/guide"
          },
          {
            text: "gitlLab",
            link: "/note/gitlab/guide"
          },
          {
            text: "css",
            children: [
              {
                text: "\u547D\u540D\u89C4\u8303",
                link: "/note/css/namespace"
              },
              {
                text: "\u6559\u7A0B\u6587\u6863",
                link: "/note/css/document"
              },
              {
                text: "\u95EE\u9898\u89E3\u7B54",
                link: "/note/css/issue"
              }
            ]
          }
        ]
      }],
      "/Vite-Vue3-TypeScript-Electron": [{
        children: [{
          text: "Electron",
          link: "/Vite-Vue3-TypeScript-Electron/electron/guide"
        }]
      }]
    }
  }),
  plugins: [
    registerComponentsPlugin({
      componentsDir: path.resolve(__dirname, "./components"),
      components: {
        threejsDemo1: path.resolve(__dirname, "./components/threejs/demo1.vue")
      }
    }),
    containerPlugin({
      type: "demo"
    }),
    containerPlugin({
      type: "doc"
    }, {
      type: "full"
    }),
    pwaPlugin({
      skipWaiting: true
    }),
    backToTopPlugin(),
    "vuepress-plugin-baidu-autopush",
    mdEnhancePlugin({
      tabs: true
    })
  ]
};
export {
  config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsiZG9jcy8udnVlcHJlc3MvY29uZmlnLmpzIl0sCiAgInNvdXJjZXNDb250ZW50IjogWyJjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfZGlybmFtZSA9IFwiRDovV29ya3MvYmxvZy9kb2NzLy52dWVwcmVzc1wiO2NvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9maWxlbmFtZSA9IFwiRDpcXFxcV29ya3NcXFxcYmxvZ1xcXFxkb2NzXFxcXC52dWVwcmVzc1xcXFxjb25maWcuanNcIjtjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfaW1wb3J0X21ldGFfdXJsID0gXCJmaWxlOi8vL0Q6L1dvcmtzL2Jsb2cvZG9jcy8udnVlcHJlc3MvY29uZmlnLmpzXCI7aW1wb3J0IHtcclxuXHRkZWZhdWx0VGhlbWVcclxufSBmcm9tICdAdnVlcHJlc3MvdGhlbWUtZGVmYXVsdCdcclxuaW1wb3J0IHtcclxuXHRnZXREaXJuYW1lLFxyXG5cdHBhdGhcclxufSBmcm9tICdAdnVlcHJlc3MvdXRpbHMnXHJcbmltcG9ydCB7XHJcblx0cmVnaXN0ZXJDb21wb25lbnRzUGx1Z2luXHJcbn0gZnJvbSAnQHZ1ZXByZXNzL3BsdWdpbi1yZWdpc3Rlci1jb21wb25lbnRzJ1xyXG5pbXBvcnQge1xyXG5cdHB3YVBsdWdpblxyXG59IGZyb20gJ0B2dWVwcmVzcy9wbHVnaW4tcHdhJ1xyXG5pbXBvcnQge1xyXG5cdGNvbnRhaW5lclBsdWdpblxyXG59IGZyb20gJ0B2dWVwcmVzcy9wbHVnaW4tY29udGFpbmVyJ1xyXG5pbXBvcnQge1xyXG5cdGJhY2tUb1RvcFBsdWdpblxyXG59IGZyb20gJ0B2dWVwcmVzcy9wbHVnaW4tYmFjay10by10b3AnXHJcblxyXG5pbXBvcnQgZW5oYW5jZSBmcm9tICd2dWVwcmVzcy1wbHVnaW4tbWQtZW5oYW5jZSc7XG5jb25zdCB7IG1kRW5oYW5jZVBsdWdpbiB9ID0gZW5oYW5jZTtcclxuY29uc3QgX19kaXJuYW1lID0gZ2V0RGlybmFtZShcclxuXHRpbXBvcnQubWV0YS51cmwpXHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuXHRsYW5nOiAnemgtQ04nLFxyXG5cdHRpdGxlOiAnXHU2NzRFXHU2QjIzXHU3NDJBSHNpbmt5TGknLFxyXG5cdGRlc2NyaXB0aW9uOiAnXHU2NzRFXHU1M0M5XHU1M0M5LFx1Njc0RVx1NkIyM1x1NzQyQSxuZXd1aSxuZXctdWksdnVlLHJlYWN0LGphdmFzY3JpcHQsdWlcdTVFOTMsdW5pYXBwLHVuaS1hcHAsdXZpZXcsZ3JhY2V1aSxpdmlldyx2YW50LG11aSxjb2xvcnVpJyxcclxuXHRoZWFkOiBbXHJcblx0XHRbJ21ldGEnLCB7XHJcblx0XHRcdG5hbWU6ICdrZXl3b3JkcycsXHJcblx0XHRcdGNvbnRlbnQ6ICdcdTY3NEVcdTUzQzlcdTUzQzksXHU2NzRFXHU2QjIzXHU3NDJBLG5ld3VpLG5ldy11aSx2dWUscmVhY3QsamF2YXNjcmlwdCx1aVx1NUU5Myx1bmlhcHAsdW5pLWFwcCx1dmlldyxncmFjZXVpLGl2aWV3LHZhbnQsbXVpLGNvbG9ydWknXHJcblx0XHR9XSxcclxuXHRcdFsnbGluaycsIHtcclxuXHRcdFx0cmVsOiAnbWFuaWZlc3QnLFxyXG5cdFx0XHRocmVmOiAnL21hbmlmZXN0LndlYm1hbmlmZXN0J1xyXG5cdFx0fV0sXHJcblx0XHRbJ21ldGEnLCB7XHJcblx0XHRcdG5hbWU6ICd0aGVtZS1jb2xvcicsXHJcblx0XHRcdGNvbnRlbnQ6ICcjM2VhZjdjJ1xyXG5cdFx0fSwgXSxcclxuXHRcdFsnbWV0YScsIHtcclxuXHRcdFx0bmFtZTogJ2JhaWR1LXNpdGUtdmVyaWZpY2F0aW9uJyxcclxuXHRcdFx0Y29udGVudDogJ2NvZGUtMlJzRHJ3OGpsMidcclxuXHRcdH1dLFxyXG5cdFx0WydsaW5rJywge1xyXG5cdFx0XHRyZWw6ICdpY29uJyxcclxuXHRcdFx0aHJlZjogJ2h0dHBzOi8vZ2l0bGFiLmNvbS91cGxvYWRzLy0vc3lzdGVtL3VzZXIvYXZhdGFyLzM3MzAyMTkvYXZhdGFyLnBuZydcclxuXHRcdH1dLFxyXG5cdF0sXHJcblx0dGhlbWU6IGRlZmF1bHRUaGVtZSh7XHJcblx0XHRsb2dvOiAnaHR0cHM6Ly9naXRsYWIuY29tL3VwbG9hZHMvLS9zeXN0ZW0vdXNlci9hdmF0YXIvMzczMDIxOS9hdmF0YXIucG5nJyxcclxuXHRcdGNvbnRyaWJ1dG9yczogZmFsc2UsXHJcblx0XHRsYXN0VXBkYXRlZDogZmFsc2UsXHJcblx0XHRzaWRlYmFyRGVwdGg6IDEsXHJcblx0XHRuYXZiYXI6IFt7XHJcblx0XHRcdFx0dGV4dDogJ1x1OTk5Nlx1OTg3NScsXHJcblx0XHRcdFx0bGluazogJy8nXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHR0ZXh0OiAnTmV3VUknLFxyXG5cdFx0XHRcdGNoaWxkcmVuOiBbe1xyXG5cdFx0XHRcdFx0XHR0ZXh0OiAnXHU2MzA3XHU1MzU3JyxcclxuXHRcdFx0XHRcdFx0bGluazogJy9uZXdVSS9pbmZvL2d1aWRlJyxcclxuXHRcdFx0XHRcdFx0YWN0aXZlTWF0Y2g6ICdeL25ld1VJL2luZm8vJyxcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdHRleHQ6ICdcdTdFQzRcdTRFRjYnLFxyXG5cdFx0XHRcdFx0XHRsaW5rOiAnL25ld1VJL2NvbXBvbmVudC9jYXJkJyxcclxuXHRcdFx0XHRcdFx0YWN0aXZlTWF0Y2g6IGBeL25ld1VJL2NvbXBvbmVudC9gLFxyXG5cdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0dGV4dDogJ0pTXHU1RTkzJyxcclxuXHRcdFx0XHRcdFx0bGluazogJy9uZXd1aS9qc3Nkay9ndWlkZScsXHJcblx0XHRcdFx0XHRcdGFjdGl2ZU1hdGNoOiBgXi9uZXdVSS9qc3Nkay9gLFxyXG5cdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRdXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0XHR0ZXh0OiAnV2ViR0wnLFxyXG5cdFx0XHRcdGxpbms6ICcvd2ViZ2wvZ3VpZGUnLFxyXG5cdFx0XHRcdGFjdGl2ZU1hdGNoOiBgXi93ZWJnbC9gLFxyXG5cdFx0XHRcdGNoaWxkcmVuOiBbe1xyXG5cdFx0XHRcdFx0XHR0ZXh0OiAnVGhyZWVKcycsXHJcblx0XHRcdFx0XHRcdGxpbms6ICcvd2ViZ2wvdGhyZWVqcy9ndWlkZScsXHJcblx0XHRcdFx0XHRcdGFjdGl2ZU1hdGNoOiAnXi93ZWJnbC90aHJlZWpzLycsXHJcblx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHR0ZXh0OiAnQmFieWxvbkpzJyxcclxuXHRcdFx0XHRcdFx0bGluazogJy93ZWJnbC9iYWJ5bG9uanMvZ3VpZGUnLFxyXG5cdFx0XHRcdFx0XHRhY3RpdmVNYXRjaDogYF4vd2ViZ2wvYmFieWxvbmpzL2AsXHJcblx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdF1cclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdHRleHQ6ICd1bmktYXBwJyxcclxuXHRcdFx0XHRsaW5rOiAnL3VuaWFwcC9ndWlkZScsXHJcblx0XHRcdFx0YWN0aXZlTWF0Y2g6IGBeL3VuaWFwcC9gLFxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0dGV4dDogJ1ZpdGUtVnVlMy1UeXBlU2NyaXB0LUVsZWN0cm9uJyxcclxuXHRcdFx0XHRsaW5rOiAnL1ZpdGUtVnVlMy1UeXBlU2NyaXB0LUVsZWN0cm9uL2d1aWRlJyxcclxuXHRcdFx0XHRhY3RpdmVNYXRjaDogYF4vVml0ZS1WdWUzLVR5cGVTY3JpcHQtRWxlY3Ryb24vYCxcclxuXHRcdFx0XHQvLyBjaGlsZHJlbjogW3tcclxuXHRcdFx0XHQvLyBcdHRleHQ6ICdFbGVjdHJvbicsXHJcblx0XHRcdFx0Ly8gXHRsaW5rOiAnL1ZpdGUtVnVlMy1UeXBlU2NyaXB0LUVsZWN0cm9uL2VsZWN0cm9uL2d1aWRlJyxcclxuXHRcdFx0XHQvLyBcdGFjdGl2ZU1hdGNoOiBgXi9WaXRlLVZ1ZTMtVHlwZVNjcmlwdC1FbGVjdHJvbi9FbGVjdHJvbi9gLFxyXG5cdFx0XHRcdC8vIH1dLFxyXG5cdFx0XHR9LFxyXG5cdFx0XHR7XHJcblx0XHRcdFx0dGV4dDogJ1dlYlNvY2tldCcsXHJcblx0XHRcdFx0bGluazogJy93ZWJTb2NrZXQvZ3VpZGUnLFxyXG5cdFx0XHRcdGFjdGl2ZU1hdGNoOiBgXi93ZWJTb2NrZXQvYCxcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdHRleHQ6ICdcdTdCMTRcdThCQjAnLFxyXG5cdFx0XHRcdGxpbms6ICcvbm90ZS9ndWlkZScsXHJcblx0XHRcdFx0YWN0aXZlTWF0Y2g6IGBeL25vdGUvYCxcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHRcdHRleHQ6ICdcdTgwNTRcdTdDRkJcdTYyMTEnLFxyXG5cdFx0XHRcdGxpbms6ICcvYWJvdXQvZ3VpZGUnLFxyXG5cdFx0XHRcdGFjdGl2ZU1hdGNoOiBgXi9hYm91dC9gLFxyXG5cdFx0XHR9LFxyXG5cdFx0XSxcclxuXHRcdHNpZGViYXI6IHtcclxuXHRcdFx0Jy9uZXdVSS9pbmZvJzogW3tcclxuXHRcdFx0XHR0ZXh0OiAnTmV3IFVJXHU0RjdGXHU3NTI4XHU2MzA3XHU1MzU3JyxcclxuXHRcdFx0XHRjaGlsZHJlbjogWycvbmV3VUkvaW5mby9ndWlkZScsICcvbmV3VUkvaW5mby9pbnN0YWxsJywgJy9uZXdVSS9pbmZvL3NldHRpbmcnXHJcblx0XHRcdFx0XHQvLyB7XHJcblx0XHRcdFx0XHQvLyAgIHRleHQ6ICdcdTUyNERcdThBMDAnLFxyXG5cdFx0XHRcdFx0Ly8gICBsaW5rOiAnL25ld1VJL2luZm8vZ3VpZGUnXHJcblx0XHRcdFx0XHQvLyB9LFxyXG5cdFx0XHRcdFx0Ly8ge1xyXG5cdFx0XHRcdFx0Ly8gICB0ZXh0OiAnXHU1Qjg5XHU4OEM1XHU0RTBFXHU2NkY0XHU2NUIwJyxcclxuXHRcdFx0XHRcdC8vICAgbGluazogJy9uZXdVSS9pbmZvL2luc3RhbGwnXHJcblx0XHRcdFx0XHQvLyB9LFxyXG5cdFx0XHRcdFx0Ly8ge1xyXG5cdFx0XHRcdFx0Ly8gICB0ZXh0OiAnXHU5MTREXHU3RjZFJyxcclxuXHRcdFx0XHRcdC8vICAgbGluazogJy9uZXdVSS9pbmZvL3NldHRpbmcnXHJcblx0XHRcdFx0XHQvLyB9XHJcblx0XHRcdFx0XVxyXG5cdFx0XHR9XSxcclxuXHRcdFx0Jy9uZXdVSS9jb21wb25lbnQvJzogW3tcclxuXHRcdFx0XHR0ZXh0OiAnTmV3IFVJXHU3RUM0XHU0RUY2XHU1RTkzJyxcclxuXHRcdFx0XHRjaGlsZHJlbjogW3tcclxuXHRcdFx0XHRcdFx0dGV4dDogJ1x1NTI0RFx1OEEwMCcsXHJcblx0XHRcdFx0XHRcdGxpbms6ICcvbmV3VUkvY29tcG9uZW50L2d1aWRlJ1xyXG5cdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0dGV4dDogJ1x1OTg3NVx1OTc2Mlx1NUUwM1x1NUM0MCcsXHJcblx0XHRcdFx0XHRcdGNoaWxkcmVuOiBbe1xyXG5cdFx0XHRcdFx0XHRcdHRleHQ6ICdQYWdlIFx1OTg3NVx1OTc2MicsXHJcblx0XHRcdFx0XHRcdFx0bGluazogJy9uZXdVSS9jb21wb25lbnQvcGFnZSdcclxuXHRcdFx0XHRcdFx0fSwgXVxyXG5cdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0dGV4dDogJ1x1NTM5Rlx1NzUxRitjbGFzc1x1NEZFRVx1OTk3MFx1N0VDNFx1NEVGNicsXHJcblx0XHRcdFx0XHRcdGNoaWxkcmVuOiBbe1xyXG5cdFx0XHRcdFx0XHRcdFx0dGV4dDogJ0J1dHRvbiBcdTYzMDlcdTk0QUUnLFxyXG5cdFx0XHRcdFx0XHRcdFx0bGluazogJy9uZXdVSS9jb21wb25lbnQvYnV0dG9uJ1xyXG5cdFx0XHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0dGV4dDogJ1JhZGlvIFx1NTM1NVx1OTAwOVx1NjMwOVx1OTRBRScsXHJcblx0XHRcdFx0XHRcdFx0XHRsaW5rOiAnL25ld1VJL2NvbXBvbmVudC9yYWRpbydcclxuXHRcdFx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdHRleHQ6ICdDaGVja2JveCBcdTU5MERcdTkwMDlcdTYzMDlcdTk0QUUnLFxyXG5cdFx0XHRcdFx0XHRcdFx0bGluazogJy9uZXdVSS9jb21wb25lbnQvY2hlY2tib3gnXHJcblx0XHRcdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdFx0XVxyXG5cdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0dGV4dDogJ1x1NjI2OVx1NUM1NVx1N0VDNFx1NEVGNicsXHJcblx0XHRcdFx0XHRcdGNoaWxkcmVuOiBbXHJcblx0XHRcdFx0XHRcdFx0Jy9uZXdVSS9jb21wb25lbnQvY2FyZCcsXHJcblx0XHRcdFx0XHRcdFx0Jy9uZXdVSS9jb21wb25lbnQvaWNvbicsXHJcblx0XHRcdFx0XHRcdFx0Jy9uZXdVSS9jb21wb25lbnQvZ3JvdXAnLFxyXG5cdFx0XHRcdFx0XHRcdCcvbmV3VUkvY29tcG9uZW50L2RpdmlkZXInLFxyXG5cdFx0XHRcdFx0XHRdXHJcblx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0Ly8ge1xyXG5cdFx0XHRcdFx0Ly8gICB0ZXh0OiAnXHU2NTcwXHU2MzZFXHU3RUM0XHU0RUY2JyxcclxuXHRcdFx0XHRcdC8vICAgY2hpbGRyZW46IFt7XHJcblx0XHRcdFx0XHQvLyAgICAgdGV4dDogJ0ljb24gXHU1NkZFXHU2ODA3JyxcclxuXHRcdFx0XHRcdC8vICAgICBsaW5rOiAnL25ld1VJL2NvbXBvbmVudC9pY29uJ1xyXG5cdFx0XHRcdFx0Ly8gICB9LFxyXG5cdFx0XHRcdFx0Ly8gICB7XHJcblx0XHRcdFx0XHQvLyAgICAgdGV4dDogJ1x1NUI4OVx1ODhDNScsXHJcblx0XHRcdFx0XHQvLyAgICAgbGluazogJy9uZXdVSS9pbnN0YWxsJ1xyXG5cdFx0XHRcdFx0Ly8gICB9LFxyXG5cdFx0XHRcdFx0Ly8gICB7XHJcblx0XHRcdFx0XHQvLyAgICAgdGV4dDogJ1x1OTE0RFx1N0Y2RScsXHJcblx0XHRcdFx0XHQvLyAgICAgbGluazogJy9uZXdVSS9zZXR0aW5nJ1xyXG5cdFx0XHRcdFx0Ly8gICB9XHJcblx0XHRcdFx0XHQvLyAgIF1cclxuXHRcdFx0XHRcdC8vIH1cclxuXHRcdFx0XHRdXHJcblx0XHRcdH1dLFxyXG5cdFx0XHQnL25ld1VJL2pzc2RrLyc6IFt7XHJcblx0XHRcdFx0dGV4dDogJ05ldyBVSSBKU1x1NUU5MycsXHJcblx0XHRcdFx0Y2hpbGRyZW46IFt7XHJcblx0XHRcdFx0XHRcdHRleHQ6ICdcdTUyNERcdThBMDAnLFxyXG5cdFx0XHRcdFx0XHRsaW5rOiAnL25ld1VJL2pzc2RrL2d1aWRlJ1xyXG5cdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0dGV4dDogJ1x1NTFGRFx1NjU3MFx1NUU5MycsXHJcblx0XHRcdFx0XHRcdGNoaWxkcmVuOiBbe1xyXG5cdFx0XHRcdFx0XHRcdHRleHQ6ICdcdTY1RjZcdTk1RjQnLFxyXG5cdFx0XHRcdFx0XHRcdGNoaWxkcmVuOiBbe1xyXG5cdFx0XHRcdFx0XHRcdFx0dGV4dDogJ3RpbWVGb3JtYXQgXHU2NUY2XHU5NUY0XHU2ODNDXHU1RjBGXHU1MzE2JyxcclxuXHRcdFx0XHRcdFx0XHRcdGxpbms6ICcvbmV3VUkvanNzZGsvdGltZUZvcm1hdCdcclxuXHRcdFx0XHRcdFx0XHR9XVxyXG5cdFx0XHRcdFx0XHR9XVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdF1cclxuXHRcdFx0fV0sXHJcblx0XHRcdCcvdW5pYXBwLyc6IFt7XHJcblx0XHRcdFx0dGV4dDogJ3VuaS1wcCcsXHJcblx0XHRcdFx0Y2hpbGRyZW46IFt7XHJcblx0XHRcdFx0XHRcdHRleHQ6ICd1bmlhcHAnLFxyXG5cdFx0XHRcdFx0XHRsaW5rOiAnL3VuaWFwcC9ndWlkZSdcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdHRleHQ6ICd1bmlDbG91ZCcsXHJcblx0XHRcdFx0XHRcdGxpbms6ICcvdW5pYXBwL3VuaUNsb3VkJ1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdF1cclxuXHRcdFx0fV0sXHJcblx0XHRcdCcvbm90ZS8nOiBbe1xyXG5cdFx0XHRcdHRleHQ6ICdcdTdCMTRcdThCQjAnLFxyXG5cdFx0XHRcdGNoaWxkcmVuOiBbe1xyXG5cdFx0XHRcdFx0XHR0ZXh0OiAnXHU3RUJGXHU3QTBCJyxcclxuXHRcdFx0XHRcdFx0bGluazogJy9ub3RlL2d1aWRlJ1xyXG5cdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0dGV4dDogJ3Z1ZXByZXNzJyxcclxuXHRcdFx0XHRcdFx0bGluazogJy9ub3RlL3Z1ZXByZXNzL2d1aWRlJ1xyXG5cdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0dGV4dDogJ2dpdGxMYWInLFxyXG5cdFx0XHRcdFx0XHRsaW5rOiAnL25vdGUvZ2l0bGFiL2d1aWRlJ1xyXG5cdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0dGV4dDogJ2NzcycsXHJcblx0XHRcdFx0XHRcdGNoaWxkcmVuOiBbe1xyXG5cdFx0XHRcdFx0XHRcdFx0dGV4dDogJ1x1NTQ3RFx1NTQwRFx1ODlDNFx1ODMwMycsXHJcblx0XHRcdFx0XHRcdFx0XHRsaW5rOiAnL25vdGUvY3NzL25hbWVzcGFjZSdcclxuXHRcdFx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdHRleHQ6ICdcdTY1NTlcdTdBMEJcdTY1ODdcdTY4NjMnLFxyXG5cdFx0XHRcdFx0XHRcdFx0bGluazogJy9ub3RlL2Nzcy9kb2N1bWVudCdcclxuXHRcdFx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdHRleHQ6ICdcdTk1RUVcdTk4OThcdTg5RTNcdTdCNTQnLFxyXG5cdFx0XHRcdFx0XHRcdFx0bGluazogJy9ub3RlL2Nzcy9pc3N1ZSdcclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdF1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRdXHJcblx0XHRcdH1dLFxyXG5cdFx0XHQnL1ZpdGUtVnVlMy1UeXBlU2NyaXB0LUVsZWN0cm9uJzogW3tcclxuXHRcdFx0XHRjaGlsZHJlbjogW3tcclxuXHRcdFx0XHRcdHRleHQ6ICdFbGVjdHJvbicsXHJcblx0XHRcdFx0XHRsaW5rOiAnL1ZpdGUtVnVlMy1UeXBlU2NyaXB0LUVsZWN0cm9uL2VsZWN0cm9uL2d1aWRlJ1xyXG5cdFx0XHRcdH0sIF1cclxuXHRcdFx0fV0sXHJcblx0XHR9XHJcblx0fSksXHJcblx0cGx1Z2luczogW1xyXG5cdFx0Ly9cdTgxRUFcdTUyQThcdTVGMTVcdTUxNjV2dWVcdTdFQzRcdTRFRjZcdUZGMENodHRwczovL3YyLnZ1ZXByZXNzLnZ1ZWpzLm9yZy96aC9yZWZlcmVuY2UvcGx1Z2luL3JlZ2lzdGVyLWNvbXBvbmVudHMuaHRtbCMlRTUlQUUlODklRTglQTMlODVcclxuXHRcdHJlZ2lzdGVyQ29tcG9uZW50c1BsdWdpbih7XHJcblx0XHRcdC8vIFx1OTE0RFx1N0Y2RVx1OTg3OVxyXG5cdFx0XHRjb21wb25lbnRzRGlyOiBwYXRoLnJlc29sdmUoX19kaXJuYW1lLCAnLi9jb21wb25lbnRzJyksXHJcblx0XHRcdGNvbXBvbmVudHM6IHtcclxuXHRcdFx0XHR0aHJlZWpzRGVtbzE6IHBhdGgucmVzb2x2ZShfX2Rpcm5hbWUsICcuL2NvbXBvbmVudHMvdGhyZWVqcy9kZW1vMS52dWUnKSxcclxuXHRcdFx0fSxcclxuXHRcdH0pLFxyXG5cdFx0Ly9cdTgxRUFcdTVCOUFcdTRFNDljb250YWluZXJcclxuXHRcdGNvbnRhaW5lclBsdWdpbih7XHJcblx0XHRcdC8vIFx1OTE0RFx1N0Y2RVx1OTg3OVxyXG5cdFx0XHR0eXBlOiAnZGVtbycsXHJcblx0XHR9KSxcclxuXHRcdGNvbnRhaW5lclBsdWdpbih7XHJcblx0XHRcdC8vIFx1OTE0RFx1N0Y2RVx1OTg3OVxyXG5cdFx0XHR0eXBlOiAnZG9jJyxcclxuXHRcdH0sIHtcclxuXHRcdFx0dHlwZTogJ2Z1bGwnXHJcblx0XHR9KSxcclxuXHRcdC8vUFdBXHJcblx0XHRwd2FQbHVnaW4oe1xyXG5cdFx0XHQvLyBcdTkxNERcdTdGNkVcdTk4NzlcclxuXHRcdFx0c2tpcFdhaXRpbmc6IHRydWUsXHJcblx0XHR9KSxcclxuXHRcdGJhY2tUb1RvcFBsdWdpbigpLFxyXG5cdFx0J3Z1ZXByZXNzLXBsdWdpbi1iYWlkdS1hdXRvcHVzaCcsXHJcblx0XHRtZEVuaGFuY2VQbHVnaW4oe1xyXG5cdFx0XHQvLyBcdTZERkJcdTUyQTBcdTkwMDlcdTk4NzlcdTUzNjFcdTY1MkZcdTYzMDFcclxuXHRcdFx0dGFiczogdHJ1ZSxcclxuXHRcdH0pXHJcblx0XSxcclxufVxuIl0sCiAgIm1hcHBpbmdzIjogIjtBQUFrUTtBQUFBLEVBQ2pRO0FBQUEsT0FDTTtBQUNQO0FBQUEsRUFDQztBQUFBLEVBQ0E7QUFBQSxPQUNNO0FBQ1A7QUFBQSxFQUNDO0FBQUEsT0FDTTtBQUNQO0FBQUEsRUFDQztBQUFBLE9BQ007QUFDUDtBQUFBLEVBQ0M7QUFBQSxPQUNNO0FBQ1A7QUFBQSxFQUNDO0FBQUEsT0FDTTtBQUVQLE9BQU8sYUFBYTtBQXBCNEksSUFBTSwyQ0FBMkM7QUFxQmpOLElBQU0sRUFBRSxnQkFBZ0IsSUFBSTtBQUM1QixJQUFNLFlBQVk7QUFBQSxFQUNqQjtBQUFlO0FBQ2hCLElBQU8saUJBQVE7QUFBQSxFQUNkLE1BQU07QUFBQSxFQUNOLE9BQU87QUFBQSxFQUNQLGFBQWE7QUFBQSxFQUNiLE1BQU07QUFBQSxJQUNMLENBQUMsUUFBUTtBQUFBLE1BQ1IsTUFBTTtBQUFBLE1BQ04sU0FBUztBQUFBLElBQ1YsQ0FBQztBQUFBLElBQ0QsQ0FBQyxRQUFRO0FBQUEsTUFDUixLQUFLO0FBQUEsTUFDTCxNQUFNO0FBQUEsSUFDUCxDQUFDO0FBQUEsSUFDRCxDQUFDLFFBQVE7QUFBQSxNQUNSLE1BQU07QUFBQSxNQUNOLFNBQVM7QUFBQSxJQUNWLENBQUc7QUFBQSxJQUNILENBQUMsUUFBUTtBQUFBLE1BQ1IsTUFBTTtBQUFBLE1BQ04sU0FBUztBQUFBLElBQ1YsQ0FBQztBQUFBLElBQ0QsQ0FBQyxRQUFRO0FBQUEsTUFDUixLQUFLO0FBQUEsTUFDTCxNQUFNO0FBQUEsSUFDUCxDQUFDO0FBQUEsRUFDRjtBQUFBLEVBQ0EsT0FBTyxhQUFhO0FBQUEsSUFDbkIsTUFBTTtBQUFBLElBQ04sY0FBYztBQUFBLElBQ2QsYUFBYTtBQUFBLElBQ2IsY0FBYztBQUFBLElBQ2QsUUFBUTtBQUFBLE1BQUM7QUFBQSxRQUNQLE1BQU07QUFBQSxRQUNOLE1BQU07QUFBQSxNQUNQO0FBQUEsTUFDQTtBQUFBLFFBQ0MsTUFBTTtBQUFBLFFBQ04sVUFBVTtBQUFBLFVBQUM7QUFBQSxZQUNULE1BQU07QUFBQSxZQUNOLE1BQU07QUFBQSxZQUNOLGFBQWE7QUFBQSxVQUNkO0FBQUEsVUFDQTtBQUFBLFlBQ0MsTUFBTTtBQUFBLFlBQ04sTUFBTTtBQUFBLFlBQ04sYUFBYTtBQUFBLFVBQ2Q7QUFBQSxVQUNBO0FBQUEsWUFDQyxNQUFNO0FBQUEsWUFDTixNQUFNO0FBQUEsWUFDTixhQUFhO0FBQUEsVUFDZDtBQUFBLFFBQ0Q7QUFBQSxNQUNEO0FBQUEsTUFDQTtBQUFBLFFBQ0MsTUFBTTtBQUFBLFFBQ04sTUFBTTtBQUFBLFFBQ04sYUFBYTtBQUFBLFFBQ2IsVUFBVTtBQUFBLFVBQUM7QUFBQSxZQUNULE1BQU07QUFBQSxZQUNOLE1BQU07QUFBQSxZQUNOLGFBQWE7QUFBQSxVQUNkO0FBQUEsVUFDQTtBQUFBLFlBQ0MsTUFBTTtBQUFBLFlBQ04sTUFBTTtBQUFBLFlBQ04sYUFBYTtBQUFBLFVBQ2Q7QUFBQSxRQUNEO0FBQUEsTUFDRDtBQUFBLE1BQ0E7QUFBQSxRQUNDLE1BQU07QUFBQSxRQUNOLE1BQU07QUFBQSxRQUNOLGFBQWE7QUFBQSxNQUNkO0FBQUEsTUFDQTtBQUFBLFFBQ0MsTUFBTTtBQUFBLFFBQ04sTUFBTTtBQUFBLFFBQ04sYUFBYTtBQUFBLE1BTWQ7QUFBQSxNQUNBO0FBQUEsUUFDQyxNQUFNO0FBQUEsUUFDTixNQUFNO0FBQUEsUUFDTixhQUFhO0FBQUEsTUFDZDtBQUFBLE1BQ0E7QUFBQSxRQUNDLE1BQU07QUFBQSxRQUNOLE1BQU07QUFBQSxRQUNOLGFBQWE7QUFBQSxNQUNkO0FBQUEsTUFDQTtBQUFBLFFBQ0MsTUFBTTtBQUFBLFFBQ04sTUFBTTtBQUFBLFFBQ04sYUFBYTtBQUFBLE1BQ2Q7QUFBQSxJQUNEO0FBQUEsSUFDQSxTQUFTO0FBQUEsTUFDUixlQUFlLENBQUM7QUFBQSxRQUNmLE1BQU07QUFBQSxRQUNOLFVBQVU7QUFBQSxVQUFDO0FBQUEsVUFBcUI7QUFBQSxVQUF1QjtBQUFBLFFBYXZEO0FBQUEsTUFDRCxDQUFDO0FBQUEsTUFDRCxxQkFBcUIsQ0FBQztBQUFBLFFBQ3JCLE1BQU07QUFBQSxRQUNOLFVBQVU7QUFBQSxVQUFDO0FBQUEsWUFDVCxNQUFNO0FBQUEsWUFDTixNQUFNO0FBQUEsVUFDUDtBQUFBLFVBQ0E7QUFBQSxZQUNDLE1BQU07QUFBQSxZQUNOLFVBQVUsQ0FBQztBQUFBLGNBQ1YsTUFBTTtBQUFBLGNBQ04sTUFBTTtBQUFBLFlBQ1AsQ0FBRztBQUFBLFVBQ0o7QUFBQSxVQUNBO0FBQUEsWUFDQyxNQUFNO0FBQUEsWUFDTixVQUFVO0FBQUEsY0FBQztBQUFBLGdCQUNULE1BQU07QUFBQSxnQkFDTixNQUFNO0FBQUEsY0FDUDtBQUFBLGNBQ0E7QUFBQSxnQkFDQyxNQUFNO0FBQUEsZ0JBQ04sTUFBTTtBQUFBLGNBQ1A7QUFBQSxjQUNBO0FBQUEsZ0JBQ0MsTUFBTTtBQUFBLGdCQUNOLE1BQU07QUFBQSxjQUNQO0FBQUEsWUFDRDtBQUFBLFVBQ0Q7QUFBQSxVQUNBO0FBQUEsWUFDQyxNQUFNO0FBQUEsWUFDTixVQUFVO0FBQUEsY0FDVDtBQUFBLGNBQ0E7QUFBQSxjQUNBO0FBQUEsY0FDQTtBQUFBLFlBQ0Q7QUFBQSxVQUNEO0FBQUEsUUFpQkQ7QUFBQSxNQUNELENBQUM7QUFBQSxNQUNELGlCQUFpQixDQUFDO0FBQUEsUUFDakIsTUFBTTtBQUFBLFFBQ04sVUFBVTtBQUFBLFVBQUM7QUFBQSxZQUNULE1BQU07QUFBQSxZQUNOLE1BQU07QUFBQSxVQUNQO0FBQUEsVUFDQTtBQUFBLFlBQ0MsTUFBTTtBQUFBLFlBQ04sVUFBVSxDQUFDO0FBQUEsY0FDVixNQUFNO0FBQUEsY0FDTixVQUFVLENBQUM7QUFBQSxnQkFDVixNQUFNO0FBQUEsZ0JBQ04sTUFBTTtBQUFBLGNBQ1AsQ0FBQztBQUFBLFlBQ0YsQ0FBQztBQUFBLFVBQ0Y7QUFBQSxRQUNEO0FBQUEsTUFDRCxDQUFDO0FBQUEsTUFDRCxZQUFZLENBQUM7QUFBQSxRQUNaLE1BQU07QUFBQSxRQUNOLFVBQVU7QUFBQSxVQUFDO0FBQUEsWUFDVCxNQUFNO0FBQUEsWUFDTixNQUFNO0FBQUEsVUFDUDtBQUFBLFVBQ0E7QUFBQSxZQUNDLE1BQU07QUFBQSxZQUNOLE1BQU07QUFBQSxVQUNQO0FBQUEsUUFDRDtBQUFBLE1BQ0QsQ0FBQztBQUFBLE1BQ0QsVUFBVSxDQUFDO0FBQUEsUUFDVixNQUFNO0FBQUEsUUFDTixVQUFVO0FBQUEsVUFBQztBQUFBLFlBQ1QsTUFBTTtBQUFBLFlBQ04sTUFBTTtBQUFBLFVBQ1A7QUFBQSxVQUNBO0FBQUEsWUFDQyxNQUFNO0FBQUEsWUFDTixNQUFNO0FBQUEsVUFDUDtBQUFBLFVBQ0E7QUFBQSxZQUNDLE1BQU07QUFBQSxZQUNOLE1BQU07QUFBQSxVQUNQO0FBQUEsVUFDQTtBQUFBLFlBQ0MsTUFBTTtBQUFBLFlBQ04sVUFBVTtBQUFBLGNBQUM7QUFBQSxnQkFDVCxNQUFNO0FBQUEsZ0JBQ04sTUFBTTtBQUFBLGNBQ1A7QUFBQSxjQUNBO0FBQUEsZ0JBQ0MsTUFBTTtBQUFBLGdCQUNOLE1BQU07QUFBQSxjQUNQO0FBQUEsY0FDQTtBQUFBLGdCQUNDLE1BQU07QUFBQSxnQkFDTixNQUFNO0FBQUEsY0FDUDtBQUFBLFlBQ0Q7QUFBQSxVQUNEO0FBQUEsUUFDRDtBQUFBLE1BQ0QsQ0FBQztBQUFBLE1BQ0Qsa0NBQWtDLENBQUM7QUFBQSxRQUNsQyxVQUFVLENBQUM7QUFBQSxVQUNWLE1BQU07QUFBQSxVQUNOLE1BQU07QUFBQSxRQUNQLENBQUc7QUFBQSxNQUNKLENBQUM7QUFBQSxJQUNGO0FBQUEsRUFDRCxDQUFDO0FBQUEsRUFDRCxTQUFTO0FBQUEsSUFFUix5QkFBeUI7QUFBQSxNQUV4QixlQUFlLEtBQUssUUFBUSxXQUFXLGNBQWM7QUFBQSxNQUNyRCxZQUFZO0FBQUEsUUFDWCxjQUFjLEtBQUssUUFBUSxXQUFXLGdDQUFnQztBQUFBLE1BQ3ZFO0FBQUEsSUFDRCxDQUFDO0FBQUEsSUFFRCxnQkFBZ0I7QUFBQSxNQUVmLE1BQU07QUFBQSxJQUNQLENBQUM7QUFBQSxJQUNELGdCQUFnQjtBQUFBLE1BRWYsTUFBTTtBQUFBLElBQ1AsR0FBRztBQUFBLE1BQ0YsTUFBTTtBQUFBLElBQ1AsQ0FBQztBQUFBLElBRUQsVUFBVTtBQUFBLE1BRVQsYUFBYTtBQUFBLElBQ2QsQ0FBQztBQUFBLElBQ0QsZ0JBQWdCO0FBQUEsSUFDaEI7QUFBQSxJQUNBLGdCQUFnQjtBQUFBLE1BRWYsTUFBTTtBQUFBLElBQ1AsQ0FBQztBQUFBLEVBQ0Y7QUFDRDsiLAogICJuYW1lcyI6IFtdCn0K
