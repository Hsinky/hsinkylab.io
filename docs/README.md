---
title: 首页
home: true
heroText: null
tagline: null
pageClass: full-page site-home
description: null
head:
  - - script
    - {}
    - console.log('%c青灯为墙，旖旎为家，以梦为马，不负韶华。','color:red');
footer: MIT Licensed | Copyright © 2023-present Hsinky Li
---
<div class="home-wrap">
	<div class="content">
		<div class="left">
			<img class="avator" src="/images/lxq.jpg" style="width:60px;height:60px;border-radius:50%;object-fit: cover;"/>
			<div style="font-size:20px;font-weight:bold;letter-spacing: 4px;line-height:2rem;">嗨，您好<br/>我是李欣琪</div>
			<ul style="line-height:2rem">
				<li>性别 男</li>
				<li>12年 工作经验</li>
				<li>毕业于 电子科技大学</li>
				<li>现居 成都</li>
			</ul>
			<div class="tag-list">
				<div class="tag">创业视野</div>
				<div class="tag">产品思维</div>
				<div class="tag">技术沉淀</div>
				<div class="tag">追求卓越</div>
			</div>
		</div>
		<div class="right" style="display:flex;flex-direction: column; justify-content: space-between;">
				<div class="row1" style="display:flex">
					<span style="font-weight:bold;padding-top:6px;white-space: nowrap;">工作经历</span>
					<ul style="margin:0px;margin-left:20px;line-height:2rem;">
						<li>2011年 ~ 2016年 从事.NET/SharePoint开发</li>
						<li>2016年 ~ 2017年 从事JAVA开发</li>
						<li>2017年 ~ 至今 从事前端开发</li>
					</ul>
				</div>
				<div class="row2" style="display:flex;">
					<span style="font-weight:bold;padding-top:6px;white-space: nowrap;">兴趣爱好</span>
					<ul style="margin:0px;margin-left:20px;line-height:2rem;">
						<li>编程，改变人的生活方式</li>
						<li>摄影，记录山海、人文赠予的浪漫</li>
						<li>徒步，路就在脚下，Keep Moving ~</li>
					</ul>
			</div>
		</div>
		<div class="col3">
		<span style="font-weight:bold;padding-top:6px;white-space: nowrap;">2024年度学习计划：</span>
			<div>
				<ul>
					<li>TypeScript</li>
					<li>Vue 3</li>
					<li>wujie、micro-app</li>
					<li>Midway、Nest</li>
					<li>Electron</li>
					<li>uniapp、uniapp x</li>
					<li>Prisma</li>
					<li>tRPC</li>
					<li>完成多应用基础框架基础开发</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="link-wrap">
		<div class="item">
			<img src="/images/mpqrcode.jpg"/>
			微信公众号
		</div>
	</div>
</div>