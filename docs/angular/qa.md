# 问题与办法

## 执行 `ng` 命令时发生错误
**如执行创建组件命令`ng generate component product-alerts`时，提示`在此系统上禁止运行脚本。有关详细信息，请参阅...`**
* win10 搜索 `Windows PowerShell`
* 选择 管理员身份运行
* 输入 `set-ExecutionPolicy RemoteSigned`
* 在输出的选择中输入 `A` 即可

## CLI生成的项目结构说明
>详见[这一章](https://angular.cn/tutorial/first-app/first-app-lesson-01#step-2---review-the-files-in-the-project)
```
angular-app 应用名称
-src
	--app 应用程序的组件文件,**新组件会添加到此目录中**
		---app.component.ts描述 app-root 组件的源文件。这是应用程序中的顶级 Angular 组件
		---app.component.css 是这个组件的样式表
	--assets 包含应用程序要用到的图像
	--favicon.ico 是应用程序的图标
	--index.html 是应用程序的顶级 HTML 模板。
	--main.server.ts
	--main.ts 是应用程序开始运行的地方
	--styles.scss 是应用程序的顶级样式
-.angular 包含构建 Angular 应用程序时所需的文件
-.e2e 包含用于测试应用程序的文件
-.node_modules 包含应用程序使用的 node.js 包
-angular.json 将向应用程序构建工具描述 Angular 应用程序
-package.json 被 npm（Node 包管理器）用来运行已完成的应用程序
-tsconfig.*是向 TypeScript 编译器描述应用程序配置的文件
```

## 在指定目录生成组件
**CLI 生成的项目结构默认没有组件专有的目录(app)，且默认创建组件的命令是在`app`目录下创建，即想要在自定义`components`下统一创建公共的组件，该怎么用？**
* `ng generate component components/xx` 等同于 `ng g c compoents/xx`，此时在目录`app`下就同时创建了`components`目录与`xx`组件文件  
但是在[这一章](https://angular.cn/tutorial/first-app/first-app-lesson-01#step-2---review-the-files-in-the-project)中写道`app`目录就是Angular 应用程序的组件文件，所以按编码所需而定吧
## 简写命令集合
> [CLI命令集合](https://angular.cn/cli/generate#ng-generate)
* `ng generate component [xx]` 等同于 `ng g c [xx]`
## 不能绑定`ngForOf`等Angular指令和管道
**报错Can't bind to 'ngForOf' since it isn't a known property of...**

``` ts
//新增如下代码
import { CommonModule } from '@angular/common';
@Component({
	imports: [CommonModule],
})
```