# [CSS命名规范](https://www.jianshu.com/p/a15e62b1d37f)
> 有一个好的样式指导手册将会显著提高开发的速度，debug的速度，以及在原有代码上完成新功能的效率。然而不幸的是，大多数的CSS 代码在开发过程中毫无任何结构和命名的规范。从长远角度来看，这会导致最后产生的CSS代码库很难维护。
## BEM
> **BEM**是Block，Element，Modifier（块元素编辑器）的缩写。BEM方法确保每一个参加了同一网站开发项目的人，基于一套代码规范去开发，这样非常有利于团队成员理解彼此的代码，创建出可以复用的前端组件和前端代码，而且对于后续接手项目的同学来说，也是一件好事。
### 概念
> **块（Block）**：是一个块级元素，可以理解为组件块，比如头部是个block，内容也是block，一个block可以由多个子block组成。  
> **元素（Element）**：是block的一部分完成某种功能，element依赖于block，比如在logo中，img是logo的一个element，在菜单中，菜单项是菜单的一个element。  
> **修饰符（Modifier）**：是用来修饰block或者element的，它表示block或者element在外观或行为上的改变，例如actived。

### 命名规则
> BEM 实体名称全部是小写字母或数字。名称中的不同单词用单个连字符（-）分隔  
> BEM 元素名称和块名称之间通过两个下划线（__）分隔。  
> 布尔修饰符和其所修饰的实体名称之间通过两个连字符（–）来分隔。不使用名值对修饰符。

### 示例代码
```
<div class="nav">
  <div class="nav-left is-left">left块区域</div>
  <button class="nav__btn nav__btn--active">拥有修饰符样式的元素按钮</button>
</div>

<div class="nav-desc">
  名称中的不同单词用单个连字符（-）分隔
</div>
```