---
title: electron-builder打包
---

## 安装electron-builder
::: tabs
@tab yarn
```
yarn add electron-builder -D
```
@tab npm
```
npm install electron-builder -D
```
:::

## 修改electron/main.ts
```ts{10-15}
const createWindow = () => {
  const win = new BrowserWindow({
    show: false, //与win.once("ready-to-show")解决启动时白屏
    webPreferences: {
      devTools: true,
      nodeIntegration: true, // 渲染进程使用Node API
      contextIsolation: true, // 是否开启隔离上下文
    },
  });
  //判断当前环境是否是打包
  if (app.isPackaged) {//打包运行
    win.loadFile(path.join(__dirname, "../index.html"));
  } else {//开发运行
    win.loadURL(`${process.env["VITE_DEV_SERVER_URL"]}`);
  }
};

```
## 新增打包命令及打包配置
在package.json中的新增script及build
```json
{
  "name": "vue3-vite-typescript-electron",
  "version": "0.0.0",
  "private": true,
  "main": "dist-electron/main.js",
  "scripts": {
    "build:electron": "run-p type-check build-only && electron-builder"
  },
  "build": {
    "appId": "com.electron.desktop",
    "productName": "electron",
    "asar": true,
    "copyright": "Copyright © 2022 electron",
    "directories": {
      "output": "release/"
    },
    "files": [
      "dist"
    ],
    "mac": {
      "artifactName": "${productName}_${version}.${ext}",
      "target": [
        "dmg"
      ]
    },
    "win": {
      "target": [
        {
          "target": "nsis",
          "arch": [
            "x64"
          ]
        }
      ],
      "artifactName": "${productName}_${version}.${ext}"
    },
    "nsis": {
      "oneClick": false,
      "perMachine": false,
      "allowToChangeInstallationDirectory": true,
      "deleteAppDataOnUninstall": false
    },
    "publish": [
      {
        "provider": "generic",
        "url": "http://127.0.0.1:8080"
      }
    ],
    "releaseInfo": {
      "releaseNotes": "版本更新的具体内容"
    }
  }
}
```
## nsis 配置详解
```json
{
	"oneClick": false, // 创建一键安装程序还是辅助安装程序（默认是一键安装）
    "allowElevation": true, // 是否允许请求提升，如果为false，则用户必须使用提升的权限重新启动安装程序 （仅作用于辅助安装程序）
    "allowToChangeInstallationDirectory": true, // 是否允许修改安装目录 （仅作用于辅助安装程序）
    "installerIcon": "public/timg.ico",// 安装程序图标的路径
    "uninstallerIcon": "public/timg.ico",// 卸载程序图标的路径
    "installerHeader": "public/timg.ico", // 安装时头部图片路径（仅作用于辅助安装程序）
    "installerHeaderIcon": "public/timg.ico", // 安装时标题图标（进度条上方）的路径（仅作用于一键安装程序）
    "installerSidebar": "public/installerSiddebar.bmp", // 安装完毕界面图片的路径，图片后缀.bmp，尺寸164*314 （仅作用于辅助安装程序）
    "uninstallerSidebar": "public/uninstallerSiddebar.bmp", // 开始卸载界面图片的路径，图片后缀.bmp，尺寸164*314 （仅作用于辅助安装程序）
    "uninstallDisplayName": "${productName}${version}", // 控制面板中的卸载程序显示名称
    "createDesktopShortcut": true, // 是否创建桌面快捷方式
    "createStartMenuShortcut": true,// 是否创建开始菜单快捷方式
    "shortcutName": "SHom", // 用于快捷方式的名称，默认为应用程序名称
    "include": "script/installer.nsi",  // NSIS包含定制安装程序脚本的路径，安装过程中自行调用  (可用于写入注册表 开机自启动等操作)
    "script": "script/installer.nsi",  // 用于自定义安装程序的NSIS脚本的路径
    "deleteAppDataOnUninstall": false, // 是否在卸载时删除应用程序数据（仅作用于一键安装程序）
    "runAfterFinish": true,  // 完成后是否运行已安装的应用程序（对于辅助安装程序，应删除相应的复选框）
    "menuCategory": false, // 是否为开始菜单快捷方式和程序文件目录创建子菜单，如果为true，则使用公司名称
}
```

## 启动打包程序
```
npm run build:electron
```
## 解决打包慢
### 修改项目Electron相关的镜像
注：此方法下载依旧很慢，经常失败
项目根目录新建.npmrc文件，加入如下环境变量
```
ELECTRON_MIRROR=https://npm.taobao.org/mirrors/electron/
```
解决 nsis、winCodeSign 下载慢
在.npmrc中同样设置ELECTRON_BUILDER_BINARIES_MIRROR镜像源地址，cnpm 同样提供了对应的镜像源
```
ELECTRON_BUILDER_BINARIES_MIRROR=http://npm.taobao.org/mirrors/electron-builder-binaries/
```
[详见](https://zhuanlan.zhihu.com/p/483976136)
<valine />