---
title: 项目搭建
---

## Vue3脚手架
使用[create-vue](https://github.com/vuejs/create-vue)创建项目
```
npm create vue@3
```
### 命令选项
```
√ Project name: ... vue3-vite-typescript-electron
√ Add TypeScript? ... No / Yes    										//YES
√ Add JSX Support? ... No / Yes   										//YES
√ Add Vue Router for Single Page Application development? ... No / Yes  //YES
√ Add Pinia for state management? ... No / Yes                          //YES
√ Add Vitest for Unit Testing? ... No / Yes                             //YES
√ Add an End-to-End Testing Solution? » Cypress     					//YES
√ Add ESLint for code quality? ... No / Yes     						//YES
√ Add Prettier for code formatting? ... No / Yes   						//YES
```
### 运行命令
```
cd vue3-vite-typescript-electron
npm install
npm run lint
npm run dev
```

## 安装Electron
::: tabs
@tab yarn
```
yarn add electron -D
```
@tab npm
```
npm install electron -D
```
:::
## 安装Vite插件
在vite中支持Electron，其他不用安装
::: tabs
@tab yarn
```
yarn add vite-plugin-electron -D
```
@tab npm
```
npm install vite-plugin-electron -D
```
:::
## 配置Electron
1.package.json增加electron的入口文件  
注：默认情况下, electron 文件夹下的文件将会被构建到dist-electron
```ts
"main": "dist-electron/main.js",
```

2.vite.config.ts中  
```ts
import electron from "vite-plugin-electron";
export default defineConfig({
  plugins: [
    vue(),
    electron({
      entry: "electron/main.ts",
    }),
  ],
})

```
3.在项目根目录下新增文件electron的入口文件electron/main.ts  
```ts
import { app, BrowserWindow } from "electron";
const createWindow = () => {
  const win = new BrowserWindow({
    webPreferences: {
      devTools: true,
	  nodeIntegration: true, // 渲染进程使用NodeAPI
	  contextIsolation: true, // 是否开启隔离上下文
    },
  });
  win.loadURL(`${process.env["VITE_DEV_SERVER_URL"]}`);
};
app.whenReady().then(createWindow);
```

## 启动运行
```
npm run dev
```

<valine />