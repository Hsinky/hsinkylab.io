# 问题与办法

## 优化白屏

```ts{2,5-8}
const win = new BrowserWindow({
    show: false, //与win.once("ready-to-show")解决启动时白屏
    ...
});
win.once("ready-to-show", () => {
	if (win) {
      win.show();
    }
});
```
<valine />