---
pageClass: custom-page
---
::: doc
# button 按钮

**说明：尽可能使用原生的基础组件，所以只用了固定的样式来修饰美化，这样不会降低性能**

使用申明：
> 1. 当需要调用原生button组件中的`open-type`等事件时，请使用`button`+`class`修饰
> 2. 当不需要调用原生button组件中的`open-type`等事件时，可使用`view`+`class`修饰
> 3. 为了保证多平台按钮样式一致性，请使用`type="default"`类型的按钮，如：`<button type="default">按钮</button>`
> 4. 其他用法与uniapp的button用法一致

## 平台兼容性
| App| H5 | 微信小程序 | 支付宝小程序 | 百度小程序 | 头条小程序 | QQ小程序 |
| :----: | :---- | :----: | :----: | :----: | :----: | :----: |
| √ | √ | √ | √ | √ | √ | √ |
## 示例代码
> 基础使用
```
<button type="default" class="n-button">按钮</button>
```
> 文字按钮
```
<button type="default" class="n-button n-button-text">按钮</button>
```
> 链接按钮，颜色默认为主题色，若需修改主题，可修改`uni.scss`中重新定义`$n-main-color`值
```
<button type="default" class="n-button n-button-link">按钮</button>
```
<valine />
:::

::: demo
<demo url="/subpages/baseComponents/button" />
:::
