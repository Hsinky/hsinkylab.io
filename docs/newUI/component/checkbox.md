---
pageClass: custom-page
---
::: doc
# checkbox 复选按钮
> 该组件采用的是***原生+Class***方式

## 平台兼容性
| App| H5 | 微信小程序 | 支付宝小程序 | 百度小程序 | 头条小程序 | QQ小程序 |
| :----: | :---- | :----: | :----: | :----: | :----: | :----: |
| √ | √ | √ | √ | √ | √ | √ |

## 示例代码
> 默认使用
```vue
 <checkbox-group>
   <label>
     <checkbox checked="true"></checkbox>
     复选按钮
   </label>
 </checkbox-group>
```

> 设置大小
```vue
 <checkbox-group>
   <label>
     <checkbox checked="true" style="transform:scale(0.7)"></checkbox>
     复选按钮
   </label>
 </checkbox-group>
```
> round样式
```vue
 <checkbox-group>
   <label>
     <checkbox class="n-round" checked="true" style="transform:scale(0.7)"></checkbox>
     复选按钮
   </label>
 </checkbox-group>
```


> 设置颜色
>> 默认各平台的定义的颜色 
>> 若通过css实现，自有属性color则无效

***通过`radio`自有属性`color`实现***
```vue
<template>
  <label>
   <radio class="n-dot" color="red"/>单选按钮
 </label>
</template>
```

***通过全局主题样式实现***
```vue
<template>
  <label>
    <radio class="n-dot"/>单选按钮
  </label>
</template>
<style lang="scss" scoped>
//必须要有v-deep
.page::v-deep {
  radio.n-dot::after {
    background-color: $n-primary !important;
  }

  radio.n-dot[checked] .wx-radio-input,
  radio.n-dot.checked .uni-radio-input {
    background-color: $n-primary !important;
    border-color: $n-primary !important;
  }
}
</style>
```
<valine />
:::

::: demo
<demo url="/subpages/baseComponents/checkbox" />
:::
