---
pageClass: custom-page
---
::: doc
# n-button 按钮
基于uniapp的button基础上做了统一的优化
****
> 1.微信小程序设置了form-type的button只会对当前组件中的form有效

# 平台兼容性
| App| H5 | 微信小程序 | 支付宝小程序 | 百度小程序 | 头条小程序 | QQ小程序 |
| :----: | :---- | :----: | :----: | :----: | :----: | :----: |
| √ | √ | √ | √ | √ | √ | √ |

## 示例代码
> 基础使用
```
<n-button>默认按钮</n-button>
<n-button type="primary">主要按钮</n-button>
<n-button type="warn">警告按钮</n-button>
<n-button type="text">文本/图标按钮</n-button>
```
> 文本前/后标
```
<n-button type="text"  text="按钮">
	<template #prefix>前标</template>
</n-button>

<n-button type="text" text="按钮">
	<template #suffix>后标</template>
</n-button>
```

## API
### 属性
| 参数 | 说明 | 类型 | 默认值 | 可选值 |
| :----: | :---- | :----: | :----: | :----: |
| <sup style="color:red">*</sup>type | 按钮类型<br/><div style="color:red">text: 文本按钮或者图标按钮</div> | String | default | default<br/>primary<br/>warn<br/>text |
| text | 文本 | String | - | - |
| text-style | 自定义文本样式 | Object | {fontSize: '26rpx',fontWeight: 'bold'} | 与vue写法一致 |
| background-color | 按钮背景色 | String | 与type相关 | 与css相同 |
| disabled-background-color | 禁用时背景色 | String | - | - |
| disabled-color | 禁用时文本颜色 | String | - | - |
| border-width | 边框宽度 | String | 0rpx | - |
| border-color | 边框颜色 | String | - | - |
| border-radius | 边框圆角 | String | 10rpx | - |
| width | 宽度 | String | - | - |
| height | 高度 | String | - | - |
| <sup style="color:red">*</sup>hover-class | 指定按钮按下去的样式类；<div style="color:red">微信小程序：须要放到页面中或App.vue中，且不能放在scoped中，在页面中的组件A中使用，在组件A中定义样式类则无效</div> | String | 执行背景色的opacity: 0.7; | `none` |
| ... | 其他同uniapp中button | - | - | - |

### 事件
| 参数 | 说明 | 类型 | 默认值 | 可选值 |
| :----: | :---- | :----: | :----: | :----: |
| ... | 其他同uniapp中button | - | - | - |

### 插槽
| 参数 | 说明 | 类型 | 默认值 | 可选值 |
| :----: | :---- | :----: | :----: | :----: |
| prefix | text前置内容 | - | - | - |
| suffix | text后置内容 | - | - | - |

<valine />

:::

::: demo
<demo url="/subpages/baseComponents/button" />
:::

