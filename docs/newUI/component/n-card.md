---
pageClass: custom-page
---
::: doc
# n-card 卡片
> 含标题、主体、底部的卡片

## 平台兼容性
<table>
  <thead>
    <tr>
      <th style="text-align: center;">App</th>
      <th style="text-align: center;">H5</th>
      <th style="text-align: center;">微信小程序</th>
      <th style="text-align: center;">支付宝小程序</th>
      <th style="text-align: center;">百度小程序</th>
      <th style="text-align: center;">头条小程序</th>
      <th style="text-align: center;">QQ小程序</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
    </tr>
  </tbody>
</table>

## 基本使用
使用`<n-card/>`组件，指定图标对应的title属性，示例代码：
```
<n-card title="标题"></n-card>
```
## API

### 属性
<table>
  <thead>
    <tr>
      <th>参数</th>
      <th>说明</th>
      <th>类型</th>
      <th>默认值</th>
      <th>可选值</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>title</td>
      <td>头部内容</td>
      <td>String</td>
      <td>-</td>
      <td>-</td>
    </tr>
  </tbody>
</table>

<valine />
:::

::: demo
<demo url="/subpages/customerComponents/card" />
:::
