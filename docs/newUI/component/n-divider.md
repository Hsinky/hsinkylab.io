---
pageClass: custom-page
---
::: doc
# n-divider 分割线
> 水平、垂直分割线

## 平台兼容性
| App| H5 | 微信小程序 | 支付宝小程序 | 百度小程序 | 头条小程序 | QQ小程序 |
| :----: | :---- | :----: | :----: | :----: | :----: | :----: |
| √ | √ | √ | √ | √ | √ | √ |

## 基本使用
只显示线条：
```
<n-divider></n-divider>
```
中间显示文字：
```
<n-divider>中间显示文字：</n-divider>
```

## API
### 属性
| 参数 | 说明 | 类型 | 默认值 | 可选值 | 示例 |
| :----: | :---- | :----: | :----: | :----: | :----: |
| direction | 线条方向 | String | `horizontal`横向 | `vertical`竖向 | `direction="horizontal"` |
| dashed | 是否虚线 | Boolean | false | true/false | `:dashed="true"` |
| color | 颜色 | String | initial | 与css相同 | `color="red"` |

### 插槽
| 参数 | 说明 |
| :----: | :---- |
| default | 自定义线条中的内容 |

<valine />
:::

::: demo
<demo url="/subpages/customerComponents/divider" />
:::
