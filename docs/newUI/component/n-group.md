---
pageClass: custom-page
---
::: doc
# n-group 分组列表
> 含分组标题的列表
<br/>
> 分组与分组项n-group-item联合使用才有意义

### 平台兼容性
| App| H5 | 微信小程序 | 支付宝小程序 | 百度小程序 | 头条小程序 | QQ小程序 |
| :----: | :---- | :----: | :----: | :----: | :----: | :----: |
| √ | √ | √ | √ | √ | √ | √ |


### 基本使用
示例代码：
```
<n-group titl="分组标题">
  <n-group-item title="项标题"></n-group-item>
</n-group>
```

### API

#### 属性

##### Group
| 参数 | 说明 | 类型 | 默认值 | 可选值 | 示例 |
| :----: | :---- | :----: | :----: | :----: | :----: |
| title | 分组标题 | String | height:92rpx | - | `title="标题"` |



##### GroupItem
| 参数 | 说明 | 类型 | 默认值 | 可选值 | 示例 |
| :----: | :---- | :----: | :----: | :----: | :----: |
| title-icon | 标题图标，在左侧 | String/Object | height:92rpx | - | `title-icon="icon-name"` |
| title | 标题，在左侧 | String/Object | height:92rpx | - | `title="标题"` |
| description | 描述，在标题下方，宽度左侧同宽，须与content区分开 | String | - | - | `description="描述"` |
| value | 值，在标题右侧 | String/Number | - | - | `value="值"` |
| value-icon | 右侧箭头图标 | String | - | 字体图标的name | `value-icon="icon-arrowright_bold"` |
| value-float | 值显示位置<br/>left:靠近title<br/>right:与title两端对齐<br/> | String | left | left/right | `value="值" :value-float="right"` |
| content | 内容，在标题和值的下方，会铺满整行，须与description区分开 | String | - | - | `content="内容"` |
| border | 底部边框 | Boolean | true | true/false | `:border="true"` 或 `border` |
| hover-class | 点击按下去的样式类，主题$n-page-bgcolor值<br/>当 hover-class="none"或:hover-class="false"时，没有点击态效果 | String/Boolean | none | none/true/false | `hover-class="none"` |
#### 插槽
| 参数 | 说明 |
| :----: | :---- |
| title-icon | 自定义左侧标题左边的图标，如需使用，请勿定义<code>title-icon</code>参数，或赋值<code>null</code>即可 |
| title | 自定义左侧标题部分的内容，如需使用，请勿定义<code>title</code>参数，或赋值<code>null</code>即可 |
| value | 自定义左侧value的内容，如需使用，请勿定义<code>value</code>参数，或赋值<code>null</code>即可 |
| value-icon | 自定义右侧的图标，如需使用，请勿定义<code>value-icon</code>参数，或赋值<code>null</code>即可 |
| description | 自定义标题和value下方的内容，会铺满整行，如需使用，请勿定义<code>description</code>参数，或赋值<code>null</code>即可 |
| default | 自定义标题和value下方的内容，会铺满整行，如需使用，请勿定义<code>content</code>参数，或赋值<code>null</code>即可 |
| value-extra | 自定义右侧内容，当定义了此插槽，仍然可以定义`value-icon`插槽；<br/> 但，***`value-flot='right'`时无效*** |

#### 事件
##### GroupItem
| 参数 | 说明 | 参数 | 说明 |
| :----: | :---- | :----: | :---- |
| click | 点击 | - | - |

#### 疑难杂症
> 1. 在微信小程序中，子组件里面无法通过this._events获取调用组件时绑定的事件，因此hover-class需要手动设置，本想通过this._events来判断是否传递了click事件来自动装载class样式

<valine />
:::

::: demo
<demo url="/subpages/customerComponents/group" />
:::
