---
pageClass: custom-page
---
::: doc
# n-icon 图标
> 基于字体的图标集，包含了大多数常见场景的图标。
## 平台兼容性
<table>
  <thead>
    <tr>
      <th style="text-align: center;">App</th>
      <th style="text-align: center;">H5</th>
      <th style="text-align: center;">微信小程序</th>
      <th style="text-align: center;">支付宝小程序</th>
      <th style="text-align: center;">百度小程序</th>
      <th style="text-align: center;">头条小程序</th>
      <th style="text-align: center;">QQ小程序</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
    </tr>
  </tbody>
</table>

## 基本使用
使用`<n-icon/>`组件，指定图标对应的type属性，示例代码：
```
<n-icon type="photo"></n-icon>
```

## API

### 属性
<table>
  <thead>
    <tr>
      <th>参数</th>
      <th>说明</th>
      <th>类型</th>
      <th>默认值</th>
      <th>可选值</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>type</td>
      <td>图标名称，见示例<a href="#iconfont">图标集合</a></td>
      <td>String</td>
      <td>-</td>
      <td>-</td>
    </tr>
    <tr>
      <td>color</td>
      <td>图标颜色</td>
      <td>String</td>
      <td>inherit</td>
      <td>-</td>
    </tr>
    <tr>
      <td>size</td>
      <td>图标字体大小，单位rpx</td>
      <td>String | Number</td>
      <td>32rpx</td>
      <td>-</td>
    </tr>
  </tbody>
</table>

### 事件
<table>
  <thead>
    <tr>
      <th style="text-align: left;">事件名</th>
      <th style="text-align: left;">说明</th>
      <th style="text-align: left;">回调参数</th>
      <th style="text-align: left;">版本</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left;">click</td>
      <td style="text-align: left;">点击图标时触发</td>
      <td style="text-align: left;">-</td>
      <td style="text-align: left;">-</td>
    </tr>
  </tbody>
</table>

## <a id="iconfont">图标集合</a >
> 详见：[iconfont](https://www.iconfont.cn/manage/index?spm=a313x.7781069.1998910419.16&manage_type=myprojects&projectId=3226029)

<valine />
:::

::: demo
<demo url="/subpages/customerComponents/icon" />
:::
