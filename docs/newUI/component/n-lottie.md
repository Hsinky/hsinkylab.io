---
pageClass: custom-page
---
::: doc
# n-lottie 动画
> Lottie动画，通过读取json文件信息实现动画效果。  
> 在NVUE的app使用的是官方animation-view组件<u>(注：暂未兼容实现，可自行引用)</u>，其他端使用的是Lottie库
## 平台兼容性
<table>
  <thead>
    <tr>
      <th style="text-align: center;">App</th>
      <th style="text-align: center;">H5</th>
      <th style="text-align: center;">微信小程序</th>
      <th style="text-align: center;">支付宝小程序</th>
      <th style="text-align: center;">百度小程序</th>
      <th style="text-align: center;">头条小程序</th>
      <th style="text-align: center;">QQ小程序</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
    </tr>
  </tbody>
</table>

## 基本使用
示例代码：
```
<n-lottie></n-lottie>
```

## API

### 属性
<table>
  <thead>
    <tr>
      <th>参数</th>
      <th>说明</th>
      <th>类型</th>
      <th style="min-width:50px">默认值</th>
      <th>可选值</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>path</td>
      <td>动画资源地址
		  <br/>支持本地路径和网络路径(小程序需要授信白名单)
		  <p/>注：<br/>
		  >百度小程序平台path属性目前不支持远程地址，仅支持本地绝对路径）;<br/>
		  >微信小程序平台path的本地路径（import.meta.glob），只是为了API保持和官方的animation-view统一，尽量使用animationData，因为不能全路径动态加载文件，导致使用很鸡肋，如果实在要用，需要把文件放到项目根目录static下，path="@/static/文件.json"
	  </td>
      <td>String</td>
      <td>String</td>
      <td>-</td>
    </tr>
	<tr>
	  <td>animationData</td>
	  <td>动画JSON，优先级高于path</td>
	  <td>Object</td>
	  <td>-</td>
	  <td>-</td>
	</tr>
    <tr>
      <td>loop</td>
      <td>动画是否循环播放</td>
      <td>Boolean</td>
      <td>false</td>
      <td>true</td>
    </tr>
    <tr>
      <td>autoplay</td>
      <td>动画是否自动播放</td>
      <td>Boolean</td>
      <td>true</td>
      <td>false</td>
    </tr>
	<tr>
	  <td>action</td>
	  <td>动画操作</td>
	  <td>String</td>
	  <td>play</td>
	  <td>play/pause/stop</td>
	</tr>
	<tr>
	  <td>hidden</td>
	  <td>是否隐藏动画</td>
	  <td>Boolean</td>
	  <td>true</td>
	  <td>false</td>
	</tr>
	<tr>
	  <td>width</td>
	  <td>动画容器宽度</td>
	  <td>String</td>
	  <td>300px</td>
	  <td>-</td>
	</tr>
	<tr>
	  <td>height</td>
	  <td>动画容器高度</td>
	  <td>String</td>
	  <td>300px</td>
	  <td>-</td>
	</tr>
  </tbody>
</table>

### 事件
<table>
  <thead>
    <tr>
      <th style="text-align: left;">事件名</th>
      <th style="text-align: left;">说明</th>
      <th style="text-align: left;">回调参数</th>
      <th style="text-align: left;">版本</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left;">bindended</td>
      <td style="text-align: left;">当播放到末尾时触发 ended 事件（自然播放结束会触发回调，循环播放结束及手动停止动画不会触发）</td>
      <td style="text-align: left;">-</td>
      <td style="text-align: left;">-</td>
    </tr>
  </tbody>
</table>

### 方法  
> 兼容[Lottie](https://github.com/airbnb/lottie-web)的所有方法
```
//引用
<n-lottie ref="lottie" path="@/static/empty.json" width="100px"></n-lottie>
//直接使用Lottie对象
this.$refs.lottie.aniMp.goToAndPlay(0)

```

## 开发心得

### 小程序端Lottie
>使用的是[lottie-miniprogram](https://github.com/wechat-miniprogram/lottie-miniprogram)  
>- 安装：在空白目录下，执行`npm install --save lottie-miniprogram`
>- 获取文件：在lottie-miniprogram\miniprogram_dist中得到index.js和index.d.ts文件，本组件取用的是index.js
>- 修改：对index.js进行文件格式化，在文件结尾进行导出`export default module.exports;`
>- 解决警告：此时会提示`Use of eval in...is strongly discouraged as it poses security risks and may cause issues with minification`，则须在文件开头处添加如下代码
```
function nEvil(fn) {
	var Fn = Function;
	return new Fn('return ' + fn)();
}
function nEvilExecute(fn) {
	var Fn = Function;
	return new Fn('return ' + fn)()();
};
//搜索'eval('并使用如上方法进行替换
//第一处：
if (data.xf) {
	var i, len = data.xf.length;
	for (i = 0; i < len; i += 1) __expression_functions[i] = eval(
		"(function(){ return " + data.xf[i] + "}())")
}
//替换成：  
if (data.xf) {
	var i, len = data.xf.length;
	for (i = 0; i < len; i += 1) __expression_functions[i] =
		nEvilExecute("function execute(){ return " + data.xf[i] + "}")
}
//第二处：
var expression_function = eval("[function _expression_function(){" +
	val + ";scoped_bm_rt=$bm_rt}]")[0],
	
//替换成：
var expression_function = nEvil("function _expression_function(){" +
	val + ";scoped_bm_rt=$bm_rt}"),
```
### APP端Lottie
>使用的是[lottie-web](https://github.com/airbnb/lottie-web)
>在安卓真机上未出现问题可直接使用
<valine />
:::

::: demo
<demo url="/subpages/customerComponents/n-lottie" />
:::
