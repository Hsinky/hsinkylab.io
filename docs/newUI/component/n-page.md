---
pageClass: custom-page
---
::: doc
# n-page页面
**包含自定义头部、主体内容、底部的页面布局**
> ***注意事项**  
> 1.当自定义footer时，须要设置footStyle的height高度(如果与默认值相同则不需要再设置)；可采用css变量实现底部内容区等高。详见[示例](#footer)  
  <u>这样做的目的是用css实现，而不需要js捕获底部区域高度动态设置底部浮动与主体内容的间距</u>


## 平台兼容性
| APP | H5 | 微信小程序 | 支付宝小程序 | 百度小程序 | 头条小程序 | QQ小程序 |
| :----: | :----: | :----: | :----: | :----: | :----: | :----: |
| √ | √ | √ | √ | √ | √ | √ |

## API
### 属性
| 参数 | 说明 | 类型 | 默认值 | 可选值 | 示例 |
| :----: | :---- | :----: | :----: | :----: | :----: |
| foot-style | 自定义底部样式 | String/Object | height:92rpx | - | String:`foot-style="height: var(--footer_wrap);"` |
| margin | 外边距，和css单位一致 | String | 0rpx | 24rpx/0rpx 24px | `margin="24px"`  `margin="0rpx 24rpx"` |
| padding | 内边距,和css单位一致 | String | 0rpx | 24rpx/0rpx 24px | `padding="24px"`  `padding="0rpx 24rpx"` |

### 插槽
| 名称 | 说明 |
| :----: | :---- |
| header | 自定义头部 |
| default | 自定义页面主体 |
| footer | 停靠在底部的内容区域 |

**注：**
> 1.`v-slot` 只能添加在`<template>`上\
> 2.`<template v-slot="header"/>`可以简写`<template #header/>`\
> 3.**因为uniapp不支持动态插槽，所以只能用if来判断显示，当需要用if的时候，在微信小程序只能用简写的方式，而v-slot会不显示**\
> 4.组件内部通过$slots['插槽名']判断，就不需要为控制显示增加pros属性了
## 示例代码
> 基础使用
```vue
<n-page>
  <template #header>
    头部区域
  </template>
  <template #default>
    内容区域
  </template>
  <template #footer>
    底部区域
  </template>
</<n-page>
```
> <text id="footer">自定义footer</text>
通过css实现
```vue
<n-page foot-style="height: var(--footer-height);">
  <template slot="footer">
    <view class="footer__wrap">底部内容</view>
  </template>
</<n-page>
<style lang="scss" scoped>
:root {
  --footer-height: 200rpx;
}

.footer__wrap {
  height: var(--footer-height);
}
</style>
```
<valine />
:::

::: demo
<demo url="/subpages/layout/page" />
:::
