---
pageClass: custom-page
---
::: doc
# n-tabs 选项卡
> 选项卡，涉及到n-tabs、n-tabs-header、n-tabs-panel三个组件
## 平台兼容性
<table>
  <thead>
    <tr>
      <th style="text-align: center;">App</th>
      <th style="text-align: center;">H5</th>
      <th style="text-align: center;">微信小程序</th>
      <th style="text-align: center;">支付宝小程序</th>
      <th style="text-align: center;">百度小程序</th>
      <th style="text-align: center;">头条小程序</th>
      <th style="text-align: center;">QQ小程序</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
      <td style="text-align: center;">√</td>
    </tr>
  </tbody>
</table>

## 基本使用  
使用`<n-tabs/>`组件，指定图标对应的type属性，示例代码：  

<CodeGroup>
  <CodeGroupItem title="VUE3">

```bash:no-line-numbers
<n-tabs :list="list" v-model="currentTab">
	<n-tabs-panel>tabs1 content</n-tabs-panel>
	<n-tabs-panel>tabs2 content</n-tabs-panel>
</n-tabs>
```
  </CodeGroupItem>
  <CodeGroupItem title="JS">

```bash:no-line-numbers
export default{
	data(){
		return{
			list:[{text:'选项卡1'},{text:'选项卡2'}],
			currentTab:0
		}
	}
}

```

  </CodeGroupItem>
</CodeGroup>

## 自定义标题  
使用`<n-tabs/>`组件，指定图标对应的type属性，示例代码：  

<CodeGroup>
  <CodeGroupItem title="VUE3">

```bash:no-line-numbers
<n-tabs v-model="currentTab">
	<template #header>
		<!-- 必须要有这个原子view，否则会影响布局 -->
		<view class="n-flex-row n-flex-justify-around n-flex-1">
			<n-tabs-header :keyName="item.day" v-for="(item,index) in dateList" :key="index"
				:activeStyle="{'border-bottom':'2px solid'}">
				<template #header>
					<view class="n-flex-column n-text-center">
						<text>{{item.week}}</text>
						<text style="font-size: 20rpx;">{{item.day}}</text>
					</view>
				</template>
			</n-tabs-header>
		</view>
	</template>
	<!-- 也可以不要这个panel就没有选项内容，就需要调用@change或者@click事件，或者监听currentTab -->
	<n-tabs-panel :keyName="item.day" v-for="(item,index) in dateList" :key="index">
		{{item.day}}
	</n-tabs-panel>
</n-tabs>
```
  </CodeGroupItem>
  <CodeGroupItem title="JS">

```bash:no-line-numbers
<script>
	export default {
		data() {
			return {
				filter: {
					startDate: '',
					endDate: ''
				},
				currentTab: 0,
				dateList: [],
			}
		},
		watch: {
			currentTab(newValue, oldValue) {
				this.page()
			}
		},
		created() {
			let weekarr = ['日', '一', '二', '三', '四', '五', '六']
			for (let i = 0; i < 7; i++) {
				const date = new Date(Date.now() + i * 24 * 60 * 60 * 1000);
				const year = date.getFullYear();
				const month = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
				const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
				const week = date.getDay()
				this.dateList.push({
					week: i === 0 ? '今' : weekarr[week],
					day: `${date.getMonth() + 1}/${date.getDate()}`,
					value: `${year}/month/day`
				})
			}
		},
		methods: {
			page() {
				console.log(this.filter);
			}
		}
	}
</script>
```

  </CodeGroupItem>
</CodeGroup>

## API

### 属性
<table>
  <thead>
    <tr>
      <th>参数</th>
      <th>说明</th>
      <th>类型</th>
      <th>默认值</th>
      <th>可选值</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>type</td>
      <td>图标名称，见示例<a href="#iconfont">图标集合</a></td>
      <td>String</td>
      <td>-</td>
      <td>-</td>
    </tr>
    <tr>
      <td>color</td>
      <td>图标颜色</td>
      <td>String</td>
      <td>inherit</td>
      <td>-</td>
    </tr>
    <tr>
      <td>size</td>
      <td>图标字体大小，单位rpx</td>
      <td>String | Number</td>
      <td>32rpx</td>
      <td>-</td>
    </tr>
  </tbody>
</table>

### 事件
<table>
  <thead>
    <tr>
      <th style="text-align: left;">事件名</th>
      <th style="text-align: left;">说明</th>
      <th style="text-align: left;">回调参数</th>
      <th style="text-align: left;">版本</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left;">click</td>
      <td style="text-align: left;">点击图标时触发</td>
      <td style="text-align: left;">-</td>
      <td style="text-align: left;">-</td>
    </tr>
  </tbody>
</table>

<valine />
:::

::: demo
<demo url="/subpages/customerComponents/tabs" />
:::
