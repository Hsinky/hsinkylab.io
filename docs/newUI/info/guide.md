---
title: 前言
description:  New UI前言
---

<style lang="scss" scoped>
  .mpqr {
    width: 50%;
  }

  @media screen and (max-width:980px) {
    .mpqr {
      width: 100% !important;
    }
  }
</style>
## 前言

#### 初心
&emsp;&emsp;自2014年接触移动端以来，工作、学习中相继使用过[mui](https://dev.dcloud.net.cn/mui/)、[iView Weapp](https://weapp.iviewui.com/)、[Vant](https://vant-contrib.gitee.io/vant)、[uni-ui](https://uniapp.dcloud.io/component/uniui/uni-ui.html)、[uView](https://www.uviewui.com/)、[Grace UI](https://www.graceui.com/)等移动端(含小程序)UI框架、[ColorUI](https://gitee.com/weilanwl/ColorUI/)CSS组件库、[Tailwind CSS](https://www.tailwindcss.cn/)，[Element UI](https://element.eleme.cn/)、[Antd](https://ant.design/)、[Naive UI](https://www.naiveui.com/)等PC端UI框架，在这期间发现每个UI框架在实际项目过程中都有一些免不了的问题，比如：应用端的兼容性不再随市场变化而迭代、架构方向的改变导致版本差异大、复用率不太高、Issue处理不及时。这当然不代表不尊重他们的劳动成果，鄙人深知开源不易！！纵使是商业性的框架，也值得让人钦佩不已！存在即合理，最难的就是从0到1的过程，请容许我在这里向各位开发者致敬！！！

&emsp;&emsp;故，这也迫使我向他们学习、研究，将综合各大家的UI框架之长，补齐其短，将自己的学习成果组合成一套符合更多场景的UI框架，应用到工作中并提高工作效率！在巨人的肩膀上持续迭代、创新！

#### 命名
&emsp;&emsp;将这套UI框架命名为**New UI**，并期望他能成为像程序员常说的<u>“New一个对象”</u>那么简单的**New一套UI**，即好看又好用！
#### 适用范围
&emsp;&emsp;New UI致力于成为uni-app生态范围内的UI框架，兼容多端，尽可能用最少的代码兼容多个终端(IOS、Android、H5、以及微信、支付宝、百度、头条、QQ、钉钉小程序)。

#### 权益事项
&emsp;&emsp;**1. New UI的功能、代码在未来可能出现与其他UI框架相似之处，若各位作者觉得侵犯您的权益，请及时联系我们。**<br/>
&emsp;&emsp;**2. New UI不可应用于违法乱纪等领域，否则，若产生法律责任，New UI不承担任何责任，并保留对其追责的权利！请各位开发者在使用前做好选择，切勿误入歧途！**

#### 联系方式
&emsp;&emsp;New UI还处于学习成长阶段，为了保证开发进度，所以暂不开设即时沟通的渠道，若您也有兴趣参与开发，或者联系我们请使用微信扫一扫下面的二维码，并留言给我们。
<p>
&emsp;&emsp;
<img :src="$withBase('/images/mpqr.png')" alt="李叉叉 微信公众号" class="mpqr"/>
</p>