---
title: 安装与更新
description:  newui;new-ui;New UI安装与更新
---

### 安装

#### 通过 uni_modules 导入全部组件
New UI遵循uni-app的uni_modules规范，可以直接通过[插件市场](https://ext.dcloud.net.cn/plugin?id=7553)`(目前仅上传了测试版本，请勿试用)`导入项目，不需要引用、注册，直接在页面中使用New UI组件。
<br/>
**注意：下载最新的组件目前仅支持uni_modules**
<br/>
如不能升级到 uni_modules 版本，可以使用 uni_modules 安装好对应组件，将组件拷贝到对应目录。

### 更新
在Hbuilder X项目中，通过右键菜单即可快速更新组件。
