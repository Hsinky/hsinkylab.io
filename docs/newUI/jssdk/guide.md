# 快速上手

## 按需引入
示例代码如下：
```
import { timeFormat } from '@/uni_modules/new-ui'
export default {
  methods: {
    timeFormat(timespan) {
      return timeFormat(timespan);
    },
  }
}
```

## 全局注入
1. 在`main.js`插入如下代码：
```js
import newUI from 'uni_modules/new-ui/install.js'
Vue.use(newUI);
```
2. 在页面中调用指定的函数
> 在`template`元素中使用：
```js
<template>
	<view>
		{{$n.timeFormat(1646376569010)}}
	</view>
</template>
```
> 在`script`中使用：
```js
export default {
  methods: {
    timeFormat(timespan) {
      return uni.$n.timeFormat(1646376569010);
    },
  }
}
```

