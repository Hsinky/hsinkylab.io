---
pageClass: custom-page
---
::: doc
# timeFormat 时间格式化

## 函数说明
<table>
  <thead>
    <tr>
      <th style="text-align: left;">参数名称</th>
      <th style="text-align: left;">参数说明</th>
      <th style="text-align: left;">参数类型</th>
      <th style="text-align: left;">参数可选值</th>
      <th style="text-align: left;">是否必须</th>
      <th style="text-align: left;">默认值</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left;">dateTime</td>
      <td style="text-align: left;">时间戳</td>
      <td style="text-align: left;">String | Number</td>
      <td style="text-align: left;"><code>秒</code>或<code>毫秒</code>时间戳</td>
      <td style="text-align: left;">是</td>
      <td style="text-align: left;">-</td>
    </tr>
    <tr>
      <td style="text-align: left;">format</td>
      <td style="text-align: left;">格式化类型</td>
      <td style="text-align: left;">String</td>
      <td style="text-align: left;"><code>yyyy:mm:dd</code> <code>yyyy:mm</code> <code>yyyy年mm月dd日</code> <code>yyyy年mm月dd日</code> <code>hh时MM分</code>等</td>
      <td style="text-align: left;">否</td>
      <td style="text-align: left;">yyyy-mm-dd</td>
    </tr>
  </tbody>
</table>

## 基本用法
### 按需引入
示例代码如下：
```
import { timeFormat } from '@/uni_modules/new-ui'
export default {
  methods: {
    timeFormat(timespan) {
      return timeFormat(timespan);
    },
  }
}
```

### 全局注入
2. 在页面中调用指定的函数
> 在`template`元素中使用：
```
<template>
	<view>
		{{$n.timeFormat(1646376569010)}}
	</view>
</template>
```
> 在`script`中使用：
```
export default {
  methods: {
    timeFormat(timespan) {
      return uni.$n.timeFormat(1646376569010);
    },
  }
}
```
<valine />
:::

::: demo
<demo url="/subpages/api/timeFormat" />
:::
