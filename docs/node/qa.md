# 问题与办法

## 修改NTFS
> 开始误以为Node可以用fs模块为指定用户设置指定权限，但看了很久API，却发现只能移交文件的所有权给指定用户，或修改当前用户的权限，这不是修改NTFS磁盘目录权限的办法，故使用node+cmd命令实现

### 使用child_process
``` ts
//引入node下的child_process模块
const { exec } = require('child_process');
//nodejs转换文件编码,使用iconv-lite插件
import iconv = require('iconv-lite');

//调用
const fpath = 'C:/Users/Desktop/tst.txt';
//给用户Hsinky对fpath附有完全控制的权限
const cmd = `icacls ${fpath} /grant Hsinky:(f)`
exec(cmd, { encoding: 'buffer' }, (error, stdout) => {
    console.log(iconv.decode(stdout, 'cp936'));//解码返回的字符串需要做进一步的解析判断
});
```
*** 当成功时输出
```
已处理的文件: C:/User/Desktop/tst.txt
已成功处理 1 个文件; 处理 0 个文件时失败
```
*** 错误时输出
```
已成功处理 0 个文件; 处理 1 个文件时失败
```
### cmd命令icacls
在cmd命令行中键入icals就能看到其用法

### 使用[node-cmd](https://www.npmjs.com/package/node-cmd)
**其实是对node的child_process简单的封装，可以看其[源码](https://www.npmjs.com/package/node-cmd?activeTab=explore)**
```ts
//安装
npm install node-cmd -save
//引用
const nodecmd = require('node-cmd');
//调用
const fpath = 'C:/Users/Desktop/tst.txt';
const cmd = `icacls ${fpath} /grant Hsinky:F`;
nodecmd.run(cmd, (err, data, stderr) => {
	console.log(err, data, stderr);
});
```

<valine />