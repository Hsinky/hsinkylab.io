# 枚举Enum
枚举（Enum）类型用于取值被限定在一定范围内的场景，比如一周只能有七天，颜色限定为红绿蓝等。
## 数字枚举
:::tabs
@tab TypeScript
``` typescript
enum Color{
	Yellow,
	Red,
	Blue
}
```

@tab JavaScript
``` js
var Color;
(function (Color) {
    Color[Color["Yellow"] = 0] = "Yellow";
    Color[Color["Red"] = 1] = "Red";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));

```
:::
从编译后的`JavaScript`可得，**如果未给枚举项设值，则默认是从0开始递增**,也可以自定义数值，**但不能设置相同的数值，否则会被保留重复的最后一个**。

## 字符串枚举
:::tabs
@tab TypeScript
``` typescript
enum Color{
	Yellow='Yellow',
	Red='Red',
	Blue='Blue'
}
```

@tab JavaScript
``` js
var Color;
(function (Color) {
    Color["Yellow"] = "Yellow";
    Color["Red"] = "Red";
    Color["Blue"] = "Blue";
})(Color || (Color = {}));
```
:::
