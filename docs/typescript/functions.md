---
title: 函数
---
## 函数声明
```ts
function sum(x: number, y: number): number {  
    return x + y;
}
```
## 函数表达式
```ts
let mySum: (x: number, y: number) => number = function (x: number, y: number): number {   
    return x + y;
};
```
## 用接口定义函数类型
```ts
interface SearchFunc{ 
  (source: string, subString: string): boolean;
}
```
采用函数表达式接口定义函数的方式时，对等号左侧进行类型限制，可以保证以后对函数名赋值时保证参数个数、参数类型、返回值类型不变。
## 可选参数
```ts
function buildName(firstName: string, lastName?: string) { 
    if (lastName) {      
        return firstName + ' ' + lastName;   
    } else {     
        return firstName;   
    }
}
let tomcat = buildName('Tom', 'Cat');
let tom = buildName('Tom');
```
**注* 可选参数后面不允许再出现必需参数
## 参数默认值
```ts
function buildName(firstName: string, lastName: string = 'Cat') {   
    return firstName + ' ' + lastName;
}
let tomcat = buildName('Tom', 'Cat');
let tom = buildName('Tom');
```
## 剩余参数
```ts
function push(array: any[], ...items: any[]) {   
    items.forEach(function(item) {     
        array.push(item);  
    });
}
let a = [];
push(a, 1, 2, 3);
```
## 函数重载
由于 JavaScript 是一个动态语言，我们通常会使用不同类型的参数来调用同一个函数，该函数会根据不同的参数而返回不同的类型的调用结果：
```ts
function add(x, y) { 
 return x + y;
}
add(1, 2); // 3
add("1", "2"); //"12"
```
由于 TypeScript 是 JavaScript 的超集，因此以上的代码可以直接在 TypeScript 中使用，但当 TypeScript 编译器开启 ```noImplicitAny``` 的配置项时，以上代码会提示以下错误信息：
```ts
Parameter 'x' implicitly has an 'any' type.
Parameter 'y' implicitly has an 'any' type.
```
该信息告诉我们参数 x 和参数 y 隐式具有 any 类型。为了解决这个问题，我们可以为参数设置一个类型。因为我们希望 add 函数同时支持 string 和 number 类型，因此我们可以定义一个 string | number 联合类型，同时我们为该联合类型取个别名：
```ts
type Combinable = string | number;
```
在定义完 Combinable 联合类型后，我们来更新一下 add 函数：
```ts
function add(a: Combinable, b: Combinable) {  
    if (typeof a === 'string' || typeof b === 'string') {    
     return a.toString() + b.toString();   
    }  
    return a + b;
}
```
为 add 函数的参数显式设置类型之后，之前错误的提示消息就消失了。那么此时的 add 函数就完美了么，我们来实际测试一下：
```ts
const result = add('Semlinker', ' Kakuqo');
result.split(' ');
```
在上面代码中，我们分别使用 'Semlinker' 和 ' Kakuqo' 这两个字符串作为参数调用 add 函数，并把调用结果保存到一个名为 result 的变量上，这时候我们想当然的认为此时 result 的变量的类型为 string，所以我们就可以正常调用字符串对象上的 split 方法。但这时 TypeScript 编译器又出现以下错误信息了：
```ts
Property 'split' does not exist on type 'number'.
```
很明显 number 类型的对象上并不存在 split 属性。问题又来了，那如何解决呢？这时我们就可以利用 TypeScript 提供的函数重载特性。
> **函数重载或方法重载是使用相同名称和不同参数数量或类型创建多个方法的一种能力。** 要解决前面遇到的问题，方法就是为同一个函数提供多个函数类型定义来进行函数重载，编译器会根据这个列表去处理函数的调用。

```ts
type Types = number | string
function add(a:number,b:number):number;
function add(a: string, b: string): string;
function add(a: string, b: number): string;
function add(a: number, b: string): string;
function add(a:Types, b:Types) {  
  if (typeof a === 'string' || typeof b === 'string') {  
    return a.toString() + b.toString(); 
  }  
  return a + b;
}
const result = add('Semlinker', ' Kakuqo');
result.split(' ');
```
在以上代码中，我们为 add 函数提供了多个函数类型定义，从而实现函数的重载。之后，可恶的错误消息又消失了，因为这时 result 变量的类型是 string 类型。