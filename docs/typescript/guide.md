---
title: 搭建环境
---

## 本地安装
安装最新版typescript
```ts
npm i -g typescript
```

安装ts-node
```ts
npm i -g ts-node
```

创建一个 tsconfig.json 文件
```ts
tsc --init
```

然后新建index.ts,输入相关练习代码，然后执行 ts-node index.ts

## 在线云环境
官方也提供了一个在线开发 TypeScript 的云环境——[Playground](https://www.typescriptlang.org/zh/play)。
基于它，我们无须在本地安装环境，只需要一个浏览器即可随时学习和编写 TypeScript，同时还可以方便地选择 TypeScript 版本、配置 tsconfig，并对 TypeScript 实时静态类型检测、转译输出 JavaScript 和在线执行。
而且在体验上，它也一点儿不逊色于任何本地的 IDE，对于刚刚学习 TypeScript 的我们来说，算是一个不错的选择。

## 链接
[博客教程](https://ts.xcatliu.com/advanced/enum.html)、[视频教程](https://www.bilibili.com/video/BV1wY411D79Z/?p=21&spm_id_from=pageDriver&vd_source=0a0d92fef65991511d3bf7a2ab5292bd)