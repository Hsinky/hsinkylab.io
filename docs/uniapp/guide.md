---
title: uni-app
---

## Vue自定义属性
在元素上添加`:data-xxx="变量"`

## uniapp二级Tab页面
不支持二级页面中也有tab。只支持一级，除非自己定义一个tabbar。

## 发短信验证码
uni-id可以发送，uniCloud API也可以发送。但uniCloud只发送，没有其他关联逻辑。需要自己存储，自己校验之类的。
uni-id则更丰富。不能用uniCloud去发送，用uni-id的API去校验或者登陆注册，那样不行。
同时使用uni-id发送短信时，一定要在`config.json`中配置key和secret。

## uni-id验证机制
前端调用uniCloud调用数据库时，unCloud会从storage中取出key为uni-id-token的字符串作为token调用后台验证。
后台验证：就是查询uni-id-users表的token字段。

## 把uni-id放入公共模块
> 1. 若cloudfunctions下没有common目录，在cloudfunctions下创建common目录。
> 2. 在common目录右键“新建公共模块目录”取名例如myutils,会自动创建index.js和package.json。不要修改package.json的name字段。
> 3. 在myutils右键单击“上传公共模块”。

## 云函数调用common模块
假设common目录下已经有一个uni-id公共模块。当在新建了一个test云函数。需要在test目录下调用uni-id。步骤如下：
> 1. 如果test目录还米有package.json则，在test目录下执行`npm init -y`生成package.json文件。
> 2. 在test目录下执行 `npm install  ../common/uni-id` 引入uni-id模块。
> 3. 在test云函数中即可用`require('uni-id')`引入

## H5跳转小程序
[参考](https://www.html5plus.org/doc/zh_cn/share.html#plus.share.ShareService.launchMiniProgram)

## mapState应用场景和作用
当一个组件需要获取多个状态的时候，将这些状态都声明为计算属性会有些重复和冗余。为了解决这个问题，我们可以使用 mapState 辅助函数帮助我们生成计算属性，让你少按几次键。

## import xx from 和import {xxx} from
若模块使用`export xxx`则用`import {xxx} from `
若模块使用`export default xxx` 则用`import xxx from`

## 返回执行上一页的指定方法
1.在跳转页使用events自定义接受事件reload
```
uni.navigateTo({
	url: '/subpages/order/activityTicket',
	events: {
		reload: function() {
			console.log(2222)
		}
	},
	success(res) {
	}
});
```
2.在返回页使用onUnload页面级事件
```
onUnload() {
// #ifdef APP-NVUE
const eventChannel = this.$scope.eventChannel; // 兼容APP-NVUE
// #endif
// #ifndef APP-NVUE
const eventChannel = this.getOpenerEventChannel();
// #endif
if (eventChannel && eventChannel.on) {
	eventChannel.emit('reload');
	}
}

```
## 打包WEB空白
修改manifest.json中的**web配置**/**运行的基础路径**为./