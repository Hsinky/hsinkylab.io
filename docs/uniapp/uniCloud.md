# uniCloud
### uniCloud使用公众号授权登陆
> 1. 云函数做后台，提供URL地址。签名在这里进行。[参考](https://blog.csdn.net/weixin_41104307/article/details/111296303)
> 2. 云函数重定向到页面
> 3. 页面需要重新载入。采用history模式。接受code后调用云函数，云函数中获取用户头像、昵称等信息。

### 云函数后段开发模式
实际项目中，很少会每个接口新建一个云函数，更常见的开发模式有如下两种：
>  1. 不写云函数，客户端直接操作数据库，开发效率更高。详见：[clientDB](https://uniapp.dcloud.io/uniCloud/database)
>  2. 使用路由框架，在一个云函数内通过控制器、路由的方式编写服务器接口，控制更灵活。插件市场有很多这类插件，[详见](https://ext.dcloud.net.cn/search?q=%E8%B7%AF%E7%94%B1&orderBy=WeekDownload&cat1=7)

### 更新数组中的一些记录
```
const res = await db.collection('uni-id-users').where({
  mobile: "17307772480"
}).update({
  // 更新'token[0]
  ['token.' + 0]: "123456789"
});

const res = await db.collection('uni-id-users').where({
  mobile: "17307772480"
}).update({
  // 更新'users[0].name为张三。
  ['users' + 0]: {
    name: "张三"
  }
});

const res = await db.collection('query').doc('1').update({
  // 更新students[1]
  ['students.' + 1]: {
    name: 'wang'
  }
})
```
### 更新数组内的操作
```
db.collection("uni-id-users").doc("5ff6d2e8fce5d000011eba74" ).set({ token:["123456"]});
```
此操作，将导致用户信息中只剩下token。其他字段被全部干掉。切记小心用。
要用的话，可以这样。
```
db.collection("uni-id-users").doc("5ff7b3ff5c2b5100018e7591" ).update({ token:["123456"]});
```

### 为乘客匹配司机的算法
> 1. 界面上司机选点。调用地图API规划路线得到n个坐标点。
> 2. 将n个坐标点，按照比例进行稀释。最终稀释为m个坐标点。
> 3. 将一条路线的m个点散落到t表中成为m条记录。这m条记录就组成某个司机的一条路线。
> 4. 封装uniCloud的getNear或者自己实现的距离计算函数。传入一个点和距离，即可查询指定距离内升序或降序的点集合。
> 5. 调用（4）传入起点，按照司机id去重。得到信息集合list（起点和哪些司机路线的哪个点，距离是多少）
> 6. 调用（4）传入终点，按照司机id去重。得到信息集合list2（终点和哪些司机路线的哪个点，距离是多少）
> 7. 整理list和list2。采用标准差方式，优先选出用户起始点和终点都离司机路线最近或者是司机路线子集的路线。得到集合list3。
> 8. 将list3按照优先级结果展示给用户。界面给出友好提示。
> 9. 结果分为四类。开头和结尾都很近，开头近结尾远，开头远结尾近，开头和结尾都远。



### 把多个文档变为一个文档的办法
> 用bucket
```
const $ = db.command.aggregate
let res = await db.collection('testlines').aggregate()
  .addFields({
    groupname: "data"
  })
  .bucket({
    groupBy: "$groupname',
    boundaries: ["data", "hello"],
    default: 'other',
    output: {
      points: $.push('$point')
    }
  })
  .end()
```
> mergedVolume

### where查询不同云空间结果不一样
> 阿里云空间里用的时候返回：`{ affectedDocs: 0, data: [] }`
> 腾讯云空间返回:`{ data: [], requestId: 'xxx' }`
所以最好的办法，不要一来其他字段。只一来data即可。万一以后要迁移数据库，蛋疼。

### auth.uid == doc.uid带来的坑
前端必须要带上查询。查询符合auth.uid == doc.uid的记录。
只校验客户端条件下的结果。

### geoNear 有时候查不到数据
重建地理位置索引即可

### 如何加密才能和数据库密码一致
使用`uniID.encryptPwd('123456')`

### mongoDB API add返回结果
`{ id: '5ff69f27fce5d000011e53fc' }`,且是个promise。

### mongoDB API wher().get()返回结果
`{ affectedDocs: 0, data: [] }`,且是个promise。

示例代码：
```
  const user = db.collection("uni-id-users");
  let res = await user.where({
    wx_openid: {
      "pb-weixin": "oPZTS5ubIzPFuQZArHwcQsQuojq4"
    }
  }).get();
```

### 公共模块里操作数据库
`const db = uniCloud.database();`
唯一需要注意的时候，公共模块或者云函数中的DB Schemal不生效。只在ClientB调用时生效。