---
title: Nuxt
---
# Nuxt
## 问题与办法
### 创建项目时发生错误
**执行创建组件命令`npx nuxi@latest init <project-name>`时，提示`ERROR  Error: Failed to download template from registry: fetch failed`**
* 在www.ipaddress.com 这个网站中的查询框中输入：raw.githubusercontent.com，找到相应的的ipv4地址
* 在Window命令窗口下，任选一个ipv4地址，并测试该ipv4地址可否ping通
* 将该ipv4地址添加到C:\Windows\System32\drivers\etc\host文件里,并保存
* 重新执行`npx nuxi@latest init <project-name>`命令，此时可以正常执行
> 建议用pnpm，因为快、顺畅一些