[[toc]]

# VuePress 2.x


## 二级域名
例如：使用简短域名username.gitlab.io访问
> 1. 修改已创建的项目
  `Settings > General > 展开Advanced > Change path`修改为自己的username.gitlab.io即可
> 2. 创建新项目
  详见[手册](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html)
  
## 添加选项卡 
注： 使用[vuepress-plugin-md-enhance](https://vuepress-theme-hope.gitee.io/v2/md-enhance/zh/)添加选项卡
:::tabs
@tab tab1
tab1 content

@tab tab2
tab2 content

:::
注：每个tab内容后需要换行才能显示出来
```
:::tabs
@tab tab1
tab1 content

@tab tab2
tab2 content

:::
```

## [PWA](https://devpress.csdn.net/vue/62fd4db97e66823466191dee.html)

## 访问静态资源
将文件放入**public**文件夹下即可。