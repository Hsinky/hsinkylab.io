---
title: WebSocket的使用
---
## 含义及其特点?
1. WebSocket是一种在单个TCP连接上进行全双工通信协议，即数据可以同时双向传输；  
2. WebSocket是一种持久化网络通信协议，一次握手即可建立长连接（握手使用HTTP协议，所以不容易被屏蔽，能通过各种HTTP代理服务器），与HTTP协议不同，HTTP需要三次握手，并且无状态，即每次通信请求都是唯一的，通过session、cookie配对等方式来认识每次请求；  
3. WebSocket可以实现服务器主动推送数据给客户端，即可以做到服务器端与客户端的实时通信，与HTTP协议不同，HTTP需要每次客户端发给请求，服务器端才会给出响应，不过基于HTTP也可实现实时通信技术，例如：轮询、长轮询、流化技术(不过有弊端)；  
4. 可以发送文本、二进制数据，数据格式比较轻量，性能开销小，通信高效；  
5. 不受同源策略限制，因此不必理会跨域问题；  
6. 与HTTP协议一样有着良好的兼容性，默认端口是80和443，并且握手阶段采用 HTTP 协议，因此握手时不容易屏蔽，能通过各种 HTTP 代理服务器；  
7. 协议标识符是ws（如果加密，则为wss），服务器网址就是 URL（如：ws://example.com:80/some/path）。

## 应用范围
WebSocket的最显著的应用特点就是实时，像常见的微信、QQ聊天室，股票基金走势等等，这些注重于实时数据的接收与发送都可以使用WebSocket。

## 客户端API
注：实例对象的所有属性和方法清单，参见[手册](https://developer.mozilla.org/zh-CN/docs/Web/API/WebSocket)
```javascript
/*判断浏览器是否支持*/
//方法一
if (typeof WebSocket != 'undefined') {
    console.log("您的浏览器支持Websocket通信协议")
}else{
    alert("您的浏览器不支持Websocket通信协议，请使用Chrome或者Firefox浏览器！")
}
//方法二
if (!!window.WebSocket && window.WebSocket.prototype.send) ｛
     console.log("您的浏览器支持Websocket通信协议")
}else{
    alert("您的浏览器不支持Websocket通信协议，请使用Chrome或者Firefox浏览器！")
}

```

## 属性
| 名称  | 说明 |
| ------------- | ------------- |
| WebSocket.binaryType  | 使用二进制的数据类型连接。  |
| WebSocket.bufferedAmount *只读*  | 未发送至服务器的字节数。表示还有多少字节的二进制数据没有发送出去。它可以用来判断发送是否结束。  |
| WebSocket.extensions *只读*  | 服务器选择的扩展。  |
| WebSocket.onclose  | 用于指定连接关闭后的回调函数。  |
| WebSocket.onerror  | 用于指定连接失败后的回调函数。  |
| WebSocket.onmessage  | 用于指定当从服务器接受到信息时的回调函数。  |
| WebSocket.onopen  | 用于指定连接成功后的回调函数。  |
| WebSocket.protocol *只读*  | 服务器选择的下属协议。  |
| WebSocket.readyState *只读*  | 当前的链接状态。共4种:CONNECTING：值为0，表示正在连接。OPEN：值为1，表示连接成功，可以通信了。CLOSING：值为2，表示连接正在关闭。CLOSED：值为3，表示连接已经关闭，或者打开连接失败。 |
| WebSocket.url *只读*  | WebSocket 的绝对路径。  |

## 方法
| 名称  | 说明 |
| ------------- | ------------- |
| WebSocket.close([code[, reason]])  | 关闭当前链接。  |
| WebSocket.send(data)  | 对要传输的数据进行排队。  |

## 事件
使用 addEventListener() 或将一个事件监听器赋值给本接口的 oneventname 属性，来监听下面的事件。
| 名称  | 说明 |
| ------------- | ------------- |
| close  | 当一个 WebSocket 连接被关闭时触发。 也可以通过 onclose 属性来设置。  |
| error  | 当一个 WebSocket 连接因错误而关闭时触发，例如无法发送数据时。 也可以通过 onerror 属性来设置。  |
| message  | 当通过 WebSocket 收到数据时触发。 也可以通过 onmessage 属性来设置。  |
| open  | 当一个 WebSocket 连接成功时触发。 也可以通过 onopen 属性来设置。  |

## 实例
```javascript
/**创建WebSocket连接*/
const ws = new WebSocket('ws://localhost:8080');
```
```javascript
/**readyState属性返回实例对象的当前状态，共有四种。*/
switch (ws.readyState) {
  case WebSocket.CONNECTING:
    break;
  case WebSocket.OPEN:
    // do something
    break;
  case WebSocket.CLOSING:
    // do something
    break;
  case WebSocket.CLOSED:
    // do something
    break;
  default:
    // this never happens
    break;
}
```

```javascript
/**实例对象的onopen属性，用于指定连接成功后的回调函数。*/
ws.onopen = function () {
  ws.send('Hello Server!');
}
//如果要指定多个回调函数，可以使用addEventListener方法。
ws.addEventListener('open', function (event) {
  ws.send('Hello Server!');
});
```

```javascript
/**例对象的onmessage属性，用于指定收到服务器数据后的回调函数。*/
ws.onmessage = function(event) {
  var data = event.data;
  // 处理数据
};
ws.addEventListener("message", function(event) {
  var data = event.data;
  // 处理数据
});
//注意，服务器数据可能是文本，也可能是二进制数据（blob对象或Arraybuffer对象）。
ws.onmessage = function(event){
  if(typeof event.data === String) {
    console.log("Received data string");
  }
  if(event.data instanceof ArrayBuffer){
    var buffer = event.data;
    console.log("Received arraybuffer");
  }
}
//除了动态判断收到的数据类型，也可以使用binaryType属性，显式指定收到的二进制数据类型。
// 收到的是 blob 数据
ws.binaryType = "blob";
ws.onmessage = function(e) {
  console.log(e.data.size);
};
// 收到的是 ArrayBuffer 数据
ws.binaryType = "arraybuffer";
ws.onmessage = function(e) {
  console.log(e.data.byteLength);
};
```

```javascript
/**实例对象的send()方法用于向服务器发送数据。*/
//发送文本的例子
ws.send('your message');
//发送 Blob 对象的例子。
var file = document.querySelector('input[type="file"]').files[0];
ws.send(file);
//发送 ArrayBuffer 对象的例子。
var img = canvas_context.getImageData(0, 0, 400, 320);
var binary = new Uint8Array(img.data.length);
for (var i = 0; i < img.data.length; i++) {
  binary[i] = img.data[i];
}
ws.send(binary.buffer);
```

```javascript
/**实例对象的bufferedAmount属性，表示还有多少字节的二进制数据没有发送出去。它可以用来判断发送是否结束。*/
var data = new ArrayBuffer(10000000);
socket.send(data);
if (socket.bufferedAmount === 0) {
  // 发送完毕
} else {
  // 发送还没结束
}
```

```javascript
/**实例对象的onerror属性，用于指定报错时的回调函数。*/
socket.onerror = function(event) {
  // handle error event
};

socket.addEventListener("error", function(event) {
  // handle error event
});
```

```javascript
/**实例对象的onclose属性，用于指定连接关闭后的回调函数。*/
ws.onclose = function(event) {
  var code = event.code;
  var reason = event.reason;
  var wasClean = event.wasClean;
  // handle close event
};
ws.addEventListener("close", function(event) {
  var code = event.code;
  var reason = event.reason;
  var wasClean = event.wasClean;
  // handle close event
});
```
## Vite+VUE实例
```javascript
<script>
const account = {
  accountId: 10001003,
  group: "无锡屏幕",
};
export default {
  data() {
    return {
      tabs: {
        current: null,
        list: [
          { label: "tab1", value: "1" },
          { label: "tab2", value: "2" },
          { label: "tab3", value: "3" },
        ],
      },
      list: [], //接口返回的总数据
      currentList: [], //当前需要显示的list
      ws: null, //websocket对象
      connection: false, //是否连接,
      reConnection: true, //是否重连,如关闭窗口或路由跳转时断开不需要再重连
      timer: {
        timeout: 30 * 1000, //30秒心跳时间
        target: null, //心跳定时器
      },
    };
  },
  created() {
    this.initWebSocket();
  },
  mounted() {
    let _this = this;
    window.unload = function () {
      _this.reConnection = false;
      _this.ws.close();
    };
  },
  destroyed() {
    this.ws.close();
  },
  watch: {
    connection(newval) {
      if (newval) {
        const _this = this;
        this.timer.target = setInterval(() => {
          setTimeout(() => {
            if (_this.ws) {
              _this.ws.send("ping");
            }
          }, 0);
        }, _this.timer.timeout);
      } else {
        this.clearTimer();
      }
    },
  },
  methods: {
    changeTab(item) {
      this.tabs.current = item.value;
      this.currentList = this.list.find(
        (x) => x.type === this.tabs.current
      )?.list;
    },
    clearTimer() {
      clearInterval(this.timer.target);
      Object.assign(this.$data.timer, this.$options.data().timer);
    },
    initWebSocket() {
      var date = Date.parse(new Date());
      const domain = import.meta.env.VITE_WEBSOCKET_PATH;
      const path = `${domain}/?id=${date}&accountId=${account.accountId}&group=${account.group}`;
      this.ws = new WebSocket(path);
      // 建立连接
      this.ws.onopen = () => {
        this.connection = true;
        this.ws.send("init");
      };
      // 客户端接收服务端返回的数据
      this.ws.onmessage = ({ data }) => {
        console.log(data);
        if (data && data !== "pong") {
          const result = JSON.parse(data);
          if (result.data) {
            //当返回data是就是返回了真实数据
            this.wsSendMessage(result.data);
          }
        }
      };
      // 发生错误
      this.ws.onerror = () => this.wsOnError();
      // 关闭连接
      this.ws.onclose = () => this.wsOnClose();
    },
    wsSendMessage(data) {
      // [{"list":[],"type":"WAIT_IN"},{"list":[],"type":"IN"},{"list":[],"type":"OUT"}]
      data = [
        {
          list: [
            {
              accountId: "10001003",
              carNo: "苏B44444",
              locationNos: ["A1-1-2"],
              materialNames: ["螺纹钢"],
              planNo: "OS202212080002",
              printTime: "2022-12-08 11:48:00",
              qty: 2,
              wgt: 29.034,
            },
            {
              accountId: "10001003",
              carNo: "苏B55555",
              locationNos: ["A1-1-3"],
              materialNames: ["螺纹钢2"],
              planNo: "OS202212080003",
              printTime: "2022-12-08 11:49:00",
              qty: 3,
              wgt: 34,
            },
            {
              accountId: "10001003",
              carNo: "苏B66666",
              locationNos: ["A1-1-4"],
              materialNames: ["螺纹钢3"],
              planNo: "OS202212080004",
              printTime: "2022-12-08 12:14:00",
              qty: 1,
              wgt: 25,
            },
          ],
          type: "WAIT_IN",
        },
        { list: [], type: "IN" },
        { list: [], type: "OUT" },
      ];
      if (this.tabs.current === null) {
        this.tabs.current = data[0].type;
      }
      this.list = data;
      this.currentList = data.find((x) => x.type === this.tabs.current)?.list;
    },
    wsOnClose() {
      this.clearTimer();
      if (this.reConnection) {
        this.initWebSocket();
      }
    },
    wsOnError() {
      this.clearTimer();
      if (this.reConnection) {
        this.initWebSocket();
      }
    },
  },
};
</script>
```

特别鸣谢[阮一峰](https://www.ruanyifeng.com/blog/2017/05/websocket.html)

<valine />